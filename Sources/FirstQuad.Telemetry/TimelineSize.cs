﻿using System;

namespace FirstQuad.Telemetry
{
    public class TimelineSize
    {
        public TimelineSize(int maxTimelineSize)
        {
            if (maxTimelineSize < 2 * 1024)
                throw new ArgumentException($"Timeline size must be at least 2KB (2 * 1024). Received size: {maxTimelineSize}", nameof(maxTimelineSize));

            MaxSizeFrom = (int)(maxTimelineSize * 0.75);
            MaxSizeTo = maxTimelineSize;

            // Optional, insted of throw exception
            //if (maxTimelineSize < 2 * 1024)
            //{
            //    MaxSizeFrom = (int)(2 * 1024 * 0.75);
            //    MaxSizeTo = 2 * 1024;
            //}
            //else
            //{
            //    MaxSizeFrom = (int)(maxTimelineSize * 0.75);
            //    MaxSizeTo = maxTimelineSize;
            //}
        }

        public int MaxSizeFrom { get;}
        public int MaxSizeTo { get;}
    }
}
