using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace FirstQuad.Telemetry.DiagnosticListeners
{
    internal class PropertyFetcher<T>
    {
        private readonly string _propertyName;
        private readonly Dictionary<string, PropertyFetch> _object2InnerFetcher;

        public PropertyFetcher(string propertyName)
        {
            _propertyName = propertyName;
            _object2InnerFetcher = new Dictionary<string, PropertyFetch>();
        }

        public T? Fetch(object? obj)
        {
            if (ReferenceEquals(obj, null))
                return default;

            var typeName = obj.GetType().FullName!;

            PropertyFetch? innerFetcher;
            lock (_object2InnerFetcher)
            {
                if (!_object2InnerFetcher.TryGetValue(typeName, out innerFetcher))
                {
                    var type = obj.GetType().GetTypeInfo();
                    var property = type.DeclaredProperties.FirstOrDefault(p => string.Equals(p.Name, _propertyName, StringComparison.InvariantCultureIgnoreCase));
                    if (property == null)
                    {
                        property = type.GetProperty(_propertyName);
                    }

                    innerFetcher = PropertyFetch.FetcherForProperty(property);
                    _object2InnerFetcher.Add(typeName, innerFetcher);
                }
            }
            
            return (T?)innerFetcher.Fetch(obj);
        }
    }


    // see https://github.com/dotnet/corefx/blob/master/src/System.Diagnostics.DiagnosticSource/src/System/Diagnostics/DiagnosticSourceEventSource.cs
    internal class PropertyFetch
    {
        /// <summary>
        /// Create a property fetcher from a .NET Reflection PropertyInfo class that
        /// represents a property of a particular type.
        /// </summary>
        public static PropertyFetch FetcherForProperty(PropertyInfo? propertyInfo)
        {
            if (propertyInfo == null)
            {
                // returns null on any fetch.
                return new PropertyFetch();
            }

            var typedPropertyFetcher = typeof(TypedFetchProperty<,>);
            var instantiatedTypedPropertyFetcher = typedPropertyFetcher.GetTypeInfo().MakeGenericType(
                propertyInfo.DeclaringType!, propertyInfo.PropertyType);
            return (PropertyFetch)Activator.CreateInstance(instantiatedTypedPropertyFetcher, propertyInfo)!;
        }

        /// <summary>
        /// Given an object, fetch the property that this propertyFetch represents.
        /// </summary>
        public virtual object? Fetch(object obj)
        {
            return null;
        }

        private class TypedFetchProperty<TObject, TProperty> : PropertyFetch
        {
            private readonly Func<TObject, TProperty> _propertyFetch;

            public TypedFetchProperty(PropertyInfo property)
            {
                if (property.GetMethod == null)
                    throw new NotSupportedException();

                _propertyFetch = (Func<TObject, TProperty>)property.GetMethod.CreateDelegate(typeof(Func<TObject, TProperty>));
            }

            public override object? Fetch(object obj)
            {
                if (obj is TObject o)
                {
                    return _propertyFetch(o);
                }

                return null;
            }
        }
    }

}