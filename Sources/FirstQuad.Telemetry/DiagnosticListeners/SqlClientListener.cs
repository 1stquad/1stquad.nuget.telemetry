using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.Data.SqlClient.Server;

namespace FirstQuad.Telemetry.DiagnosticListeners
{
    internal class SqlClientListener : IObserver<DiagnosticListener>, IObserver<KeyValuePair<string, object?>>
    {
        private readonly PropertyFetcher<Guid>                          _connectionIdFetcher        = new PropertyFetcher<Guid>("ConnectionId");
        private readonly PropertyFetcher<DbCommand>                     _commandFetcher             = new PropertyFetcher<DbCommand>("Command");
        private readonly PropertyFetcher<CommandType>                   _commandTypeFetcher         = new PropertyFetcher<CommandType>("CommandType");
        private readonly PropertyFetcher<Dictionary<object, object>>    _statisticsFetcher          = new PropertyFetcher<Dictionary<object, object>>("Statistics");
        private readonly PropertyFetcher<DbParameterCollection>         _commandParametersFetcher   = new PropertyFetcher<DbParameterCollection>("Parameters");
        private readonly PropertyFetcher<string>                        _commandTextFetcher         = new PropertyFetcher<string>("CommandText");
        private readonly PropertyFetcher<Exception>                     _exceptionFetcher           = new PropertyFetcher<Exception>("Exception");

        private readonly PropertyFetcher<SqlDbType>                     _sqlDbTypeFetcher           = new PropertyFetcher<SqlDbType>("SqlDbType");
        private readonly PropertyFetcher<string>                        _typenameFetcher            = new PropertyFetcher<string>("TypeName");

        private readonly List<IDisposable> _subscriptions;

        public SqlClientListener()
        {
            _subscriptions = new List<IDisposable>();
        }

        void IObserver<DiagnosticListener>.OnNext(DiagnosticListener diagnosticListener)
        {
            if (diagnosticListener.Name == "SqlClientDiagnosticListener")
            {
                var subscription = diagnosticListener.Subscribe(this);
                _subscriptions.Add(subscription);
            }
        }

        void IObserver<DiagnosticListener>.OnError(Exception error)
        { }

        void IObserver<DiagnosticListener>.OnCompleted()
        {
            _subscriptions.ForEach(x => x.Dispose());
            _subscriptions.Clear();
        }


        void IObserver<KeyValuePair<string, object?>>.OnCompleted()
        {
            _subscriptions.ForEach(x => x.Dispose());
            _subscriptions.Clear();
        }

        void IObserver<KeyValuePair<string, object?>>.OnError(Exception error)
        {
        }

        void IObserver<KeyValuePair<string, object?>>.OnNext(KeyValuePair<string, object?> value)
        {
            OnEvent(value.Key, value.Value);
        }

        private void OnEvent(string name, object? payload)
        {
            switch (name)
            {
                case "System.Data.SqlClient.WriteConnectionOpenBefore":
                case "System.Data.SqlClient.WriteConnectionOpenAfter":
                case "System.Data.SqlClient.WriteConnectionCloseBefore":
                case "System.Data.SqlClient.WriteConnectionCloseAfter":
                case "System.Data.SqlClient.WriteTransactionCommitBefore":
                case "System.Data.SqlClient.WriteTransactionCommitAfter":
                case "Microsoft.Data.SqlClient.WriteConnectionOpenBefore":
                case "Microsoft.Data.SqlClient.WriteConnectionOpenAfter":
                case "Microsoft.Data.SqlClient.WriteConnectionCloseBefore":
                case "Microsoft.Data.SqlClient.WriteConnectionCloseAfter":
                case "Microsoft.Data.SqlClient.WriteTransactionCommitBefore":
                case "Microsoft.Data.SqlClient.WriteTransactionCommitAfter":
                    break; 

                case "System.Data.SqlClient.WriteCommandBefore":
                case "Microsoft.Data.SqlClient.WriteCommandBefore":
                    {
                        // NOTE: Equals to the SqlConnection.ClientConnectionId
                        var connectionId = _connectionIdFetcher.Fetch(payload);

                        OnWriteCommandBefore(connectionId);

                        break;
                    }
                case "System.Data.SqlClient.WriteCommandAfter":
                case "Microsoft.Data.SqlClient.WriteCommandAfter":
                    {
                        // NOTE: Equals to the SqlConnection.ClientConnectionId
                        var connectionId = _connectionIdFetcher.Fetch(payload);

                        var command = _commandFetcher.Fetch(payload);
                        
                        var statistics = _statisticsFetcher.Fetch(payload);

                        var commandText = _commandTextFetcher.Fetch(command);
                        var parameters = _commandParametersFetcher.Fetch(command)?.Cast<DbParameter>().ToList();

                        var commandType = _commandTypeFetcher.Fetch(command);

                        OnCommandExecuted(
                            connectionId,
                            command?.Connection?.Database,
                            commandType == CommandType.StoredProcedure,
                            commandText,
                            parameters,
                            statistics);

                        break;
                    }
                case "System.Data.SqlClient.WriteCommandError":
                case "Microsoft.Data.SqlClient.WriteCommandError":
                    {
                        // NOTE: Equals to the SqlConnection.ClientConnectionId
                        var connectionId = _connectionIdFetcher.Fetch(payload);

                        var command = _commandFetcher.Fetch(payload);
                        var statistics = _statisticsFetcher.Fetch(payload);

                        var commandText = _commandTextFetcher.Fetch(command);
                        var parameters = _commandParametersFetcher.Fetch(command)?.Cast<DbParameter>().ToList();

                        var commandType = _commandTypeFetcher.Fetch(command);
                        var exception = _exceptionFetcher.Fetch(payload);

                        OnCommandError(
                            connectionId,
                            command?.Connection?.Database,
                            commandType == CommandType.StoredProcedure,
                            commandText,
                            parameters,
                            statistics,
                            exception);

                        break;
                    }
            }
        }

        private string? FormatSqlValue(object? value)
        {
            switch (value)
            {
                case string s:
                    return $"'{s}'";

                case Guid g:
                    return $"'{g}'";

                case DateTime d:
                    return $"'{d:s}'";

                case bool b:
                    return b ? "1" : "0";

                case DBNull _:
                    return "NULL";

                case null:
                    return "null";

                default:
                    return value.ToString();
            }
        }

        private string GenerateCommandParametersSql(
            IEnumerable<DbParameter>? parameters)
        {
            if (parameters == null)
                return string.Empty;

            var sb = new StringBuilder();
            foreach (var sqlParameter in parameters)
            {
                var sqlDbType = _sqlDbTypeFetcher.Fetch(sqlParameter);
                var sqlTypeName = _typenameFetcher.Fetch(sqlParameter);
                
                var parameterName = sqlParameter.ParameterName;
                if (!parameterName.StartsWith("@"))
                {
                    parameterName = "@" + parameterName;
                }

                if (sqlDbType == SqlDbType.Structured)
                {
                    sb.Append($"DECLARE {parameterName} {sqlTypeName}\r\n\r\n");
                    sb.Append($"INSERT INTO {parameterName} VALUES\r\n");

                    var values = ((IEnumerable<SqlDataRecord>)sqlParameter.Value!)
                        .Select(x => $"({FormatSqlValue(x.GetValue(0))})")
                        .ToList();

                    var count = values.Count;
                    if (count > 100)
                    {
                        values = values.Take(100).ToList();
                        values.Add($"[...from {count}]");
                    }

                    sb.Append(string.Join(",\r\n", values));

                    sb.Append("\r\n");
                }
                else
                {
                    var sqlType = sqlDbType + (sqlParameter.Size > 0 && sqlDbType != SqlDbType.TinyInt ? $"({sqlParameter.Size})" : "");
                    var sqlValue = FormatSqlValue(sqlParameter.Value);

                    sb.Append($"DECLARE {parameterName} {sqlType} = {sqlValue};\r\n");
                }

            }

            return sb.ToString();
        }

        private void OnCommandFinished(
            Guid connectionId,
            string? databaseName,
            bool isStoredProcedure,
            string? commandText,
            IEnumerable<DbParameter>? parameters,
            Dictionary<object, object>? statistics,
            Exception? e)
        {
            // Read time when command started
            var connectionIdString = connectionId.ToString();

            var startTimeMs = TelemetryManager.GetTimelineItemTimeMark(connectionIdString);
            var endTimeMs = TelemetryManager.GetElapsedSinceOperationStarted();

            // Save time when command finished to use it later on possible reader dispose event
            TelemetryManager.SetTimelineItemTimeMark(connectionIdString);

            TelemetryManager.IncreaseSqlOperationsCounter();
            TelemetryManager.IncreaseSqlProcessingCounter(endTimeMs - startTimeMs);

            var bytesReceived = (long?)statistics?["BytesReceived"];
            var selectRows = (long?)statistics?["SelectRows"];

            var message = string.Empty;
            if (selectRows.HasValue)
            {
                message += $"Select Rows: {selectRows}\r\n";
            }
            if (bytesReceived.HasValue)
            {
                message += $"Bytes Received: {bytesReceived}\r\n";
            }

            TelemetryManager.AddTimelineItem(
                new TimelineItem
                {
                    Text = isStoredProcedure ? "EXEC " : string.Empty + commandText,
                    Comment = message,
                    SqlParameters = GenerateCommandParametersSql(parameters),
                    StartTimeMs = startTimeMs,
                    EndTimeMs = endTimeMs,
                    Exception = e,
                    CssColor = e != null ? "red" : TelemetryConstants.SqlRequestCssColor,
                    Category = "SQL: " + databaseName
                });
        }

        public void OnWriteCommandBefore(
            Guid connectionId)
        {
            var connectionIdString = connectionId.ToString();

            TelemetryManager.SetTimelineItemTimeMark(connectionIdString);
        }

        public void OnCommandExecuted(
            Guid connectionId,
            string? databaseName,
            bool isStoredProcedure,
            string? commandText,
            IEnumerable<DbParameter>? parameters,
            Dictionary<object, object>? statistics)
        {
            OnCommandFinished(
                connectionId,
                databaseName,
                isStoredProcedure,
                commandText,
                parameters,
                statistics,
                null);
        }

        public void OnCommandError(
            Guid connectionId,
            string? databaseName,
            bool isStoredProcedure,
            string? commandText,
            IEnumerable<DbParameter>? parameters,
            Dictionary<object, object>? statistics,
            Exception? exception)
        {
            OnCommandFinished(
                connectionId,
                databaseName,
                isStoredProcedure,
                commandText,
                parameters,
                statistics,
                exception);
        }
    }
}