using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.DiagnosticAdapter;

namespace FirstQuad.Telemetry.DiagnosticListeners
{
    internal class HttpRequestDiagnosticsListener : IObserver<DiagnosticListener>
    {
        private readonly List<IDisposable> _subscriptions;

        public HttpRequestDiagnosticsListener()
        {
            _subscriptions = new List<IDisposable>();
        }

        void IObserver<DiagnosticListener>.OnNext(DiagnosticListener diagnosticListener)
        {
            if (diagnosticListener.Name == "HttpHandlerDiagnosticListener")
            {
                var subscription = diagnosticListener.SubscribeWithAdapter(this);
                _subscriptions.Add(subscription);
            }
        }

        void IObserver<DiagnosticListener>.OnError(Exception error)
        { }

        void IObserver<DiagnosticListener>.OnCompleted()
        {
            _subscriptions.ForEach(x => x.Dispose());
            _subscriptions.Clear();
        }

        [DiagnosticName("System.Net.Http.HttpRequestOut")]
        public void IsEnabled()
        {
            // FAKE event handler needed to make Start/Stop event handlers working
        }

        [DiagnosticName("System.Net.Http.HttpRequestOut.Start")]
        public void HttpRequestOutStart(
            HttpRequestMessage request)
        {
            TelemetryManager.SetTimelineItemTimeMark(request.RequestUri?.ToString() ?? "unknown URI");
        }

        [DiagnosticName("System.Net.Http.HttpRequestOut.Stop")]
        public void HttpRequestOutStop(
            HttpRequestMessage request,
            HttpResponseMessage response,
            TaskStatus requestTaskStatus)
        {
            var startTimeMs = TelemetryManager.GetTimelineItemTimeMark(request.RequestUri?.ToString() ?? "unknown URI");
            var endTimeMs = TelemetryManager.GetElapsedSinceOperationStarted();

            TelemetryManager.AddTimelineItem(
                new TimelineItem
                {
                    Text = $"Http Request {request.Method} {request.RequestUri} {response?.StatusCode}",
                    StartTimeMs = startTimeMs,
                    EndTimeMs = endTimeMs,
                    Category = "HTTP",
                    Parameters = request.Headers.Select(x => new TimelineItemParameterDescription
                    {
                        Name = x.Key,
                        Type = Enums.TelemetryItemParameterType.String,
                        Value = string.Join(';', x.Value)
                    }).ToList()
                }); 
        }


        [DiagnosticName("System.Net.Http.Exception")]
        public void HttpRequestOutException(
            Exception exception,
            HttpRequestMessage request)
        {
            TelemetryManager.AddTimelineItem(
                new TimelineItem
                {
                    Text = $"Out Http Request {request.Method} {request.RequestUri} {exception.GetType()}",
                    StartTimeMs = TelemetryManager.GetTimelineItemTimeMark(request.RequestUri?.ToString() ?? "unknown URI"),
                    EndTimeMs = TelemetryManager.GetElapsedSinceOperationStarted(),
                    Category = "HTTP",
                    Parameters = request.Headers.Select(x => new TimelineItemParameterDescription
                    {
                        Name = x.Key,
                        Type = Enums.TelemetryItemParameterType.String,
                        Value = string.Join(';', x.Value)
                    }).ToList()
                });
        }
    }
}
