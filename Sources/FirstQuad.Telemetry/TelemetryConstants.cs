namespace FirstQuad.Telemetry
{
    public class TelemetryConstants
    {
        public const string JobCssColor = "#FF0000";
        public const string SummariesColor = "#000000";
        public const string SqlRequestCssColor = "purple";
        public const string SqlDataReaderCssColor = "green";
        public const string HttpRequestCssColor = "orange";
        public const string EntityFrameworkQueryPlanned = "aqua";
        public const string ErrorCssColor = "red";
    }
}
