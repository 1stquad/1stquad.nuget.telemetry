using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace FirstQuad.Telemetry
{
    public class TelemetryTrackOptions
    {
        public string Name { get; set; } = null!;
        public Func<string, Task> OnNotifyAsync = null!;
        public string? Parameters{ get; set; }
        public string? Category { get; set; }
        public string? CssColor { get; set; }
        public int? MaxSizeBytes { get; set; }
    }
    
    public class TelemetryManager
    {
        public static void StartNewOperation()
        {
            TelemetryItemsHolder.Current = new TelemetryItemsHolder();
        }

        public static void FinishOperation()
        {
            TelemetryItemsHolder.Current.IsFinished = true;
        }

        public static void CleanupOperation()
        {
            TelemetryItemsHolder.Current = new TelemetryItemsHolder();
        }

        public static decimal GetElapsedSinceOperationStarted()
        {
            return (decimal)TelemetryItemsHolder.Current.Stopwatch.Elapsed.TotalMilliseconds;
        }

        public static int GetSqlOperationsCounter()
        {
            return TelemetryItemsHolder.Current.SqlOperationsCounter;
        }

        public static decimal GetSqlProcessingCounter()
        {
            return TelemetryItemsHolder.Current.SqlProcessingCounter;
        }

        public static decimal GetSqlBytesReceivedCounter()
        {
            return TelemetryItemsHolder.Current.SqlBytesReceivedCounter;
        }

        public static decimal GetSqlSelectRowsCounter()
        {
            return TelemetryItemsHolder.Current.SqlSelectRowsCounter;
        }

        public static void IncreaseSqlOperationsCounter()
        {
            TelemetryItemsHolder.Current.SqlOperationsCounter += 1;
        }

        public static void UpdateSqlBytesReceivedCounter(long value)
        {
            TelemetryItemsHolder.Current.SqlBytesReceivedCounter = value;
        }

        public static void UpdateSqlSelectRowsCounter(long value)
        {
            TelemetryItemsHolder.Current.SqlSelectRowsCounter = value;
        }

        public static void IncreaseSqlProcessingCounter(decimal value)
        {
            TelemetryItemsHolder.Current.SqlProcessingCounter += (int)value;
        }

        public static void AddTimelineItem(TimelineItem item)
        {
            if (item == null)
            {
                throw new Exception("Timeline item cannot be null");
            }

            item.EndTimeMs = GetElapsedSinceOperationStarted();

            var timeline = GetTimeline();

            lock(timeline)
            {
                if (timeline.IsFinished)
                {
                    Debug.WriteLine($"Timeline is finished. Item will be lost. \r\n'{item.OperationName}'\r\nText:\r\n{item.Text}");
                }
                else
                {
                	timeline.Add(item);
                }
            }
        }

        public static string GetBase64SerializedTimeline(TimelineSize timelineSize)
        {
            var timeline = GetTimeline();

            lock (timeline)
            {
                return timeline.SerializeBase64Deflate(timelineSize);
            }
        }

        public static Timeline GetTimeline()
        {
            return TelemetryItemsHolder.Current.Timeline;
        }

        public static void SetTimelineItemTimeMark(string o)
        {
            if (!ReferenceEquals(o, null))
            {
                TelemetryItemsHolder.Current.TimelineItem2StartTime.AddOrUpdate(o,
                    _ => GetElapsedSinceOperationStarted(), 
                    (_, _) => GetElapsedSinceOperationStarted());
            }
        }

        public static decimal GetTimelineItemTimeMark(string? o)
        {
            if (ReferenceEquals(o, null))
                return GetElapsedSinceOperationStarted();

            if (TelemetryItemsHolder.Current.TimelineItem2StartTime.TryGetValue(o, out decimal result))
                return result;

            // Not sure yet why it fails, but sometimes it is executed in other logical thread than actual command
            return GetElapsedSinceOperationStarted();
        }
        
        public static async Task TrackAsync(
            TelemetryTrackOptions options,
            Func<Task> action)
        {
            if (!RequestTelemetryMiddleware.IsTimelineEnabled)
            {
                await action();
                return;
            }

            StartNewOperation();
            var timelineItem = new TimelineItem
            {
                Text = options.Name,
                CssColor = options.CssColor ?? TelemetryConstants.JobCssColor,
                Category = options.Category
            };

            try
            {
                await action();
            }
            catch (Exception ex)
            {
                timelineItem.Exception = ex;
                timelineItem.CssColor = TelemetryConstants.ErrorCssColor;
                throw;
            }
            finally
            {
                AddTimelineItem(timelineItem);

                var timeline = GetTimeline();

                AddTimelineItem(new TimelineItem
                {
                    OperationName = "Summary",
                    CssColor = TelemetryConstants.SummariesColor,
                    Text = string.Join(",", timeline.CategoryCounters.Select(x => $"{x.Key ?? "~"}: {x.Value:F1}ms"))
                });

                timeline.TimelineName = options.Name;
                timeline.TimelineParameters = options.Parameters;

                var rawData = timeline.SerializeBase64Deflate(new TimelineSize(options.MaxSizeBytes ?? 1024 * 24));
                await options.OnNotifyAsync(rawData);

                FinishOperation();
            }
        }
    }
}
