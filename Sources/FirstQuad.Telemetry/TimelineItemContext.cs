using System;

namespace FirstQuad.Telemetry
{
    public class TimelineItemContext : IDisposable
    {
        private readonly TimelineItem _timelineItem;

        public TimelineItemContext(string text, string? cssClass = null, string? category = null)
        {
            _timelineItem = new TimelineItem
            {
                Text = text,
                CssColor = cssClass, // TODO: Rename
                Category = category
            };
        }

        public void OnException(Exception e)
        {
            _timelineItem.Exception = e;
        }

        public void Dispose()
        {
            TelemetryManager.AddTimelineItem(_timelineItem);
        }
    }
}
