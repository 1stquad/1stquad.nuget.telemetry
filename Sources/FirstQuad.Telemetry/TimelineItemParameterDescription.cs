﻿using FirstQuad.Telemetry.Enums;

namespace FirstQuad.Telemetry
{
    public class TimelineItemParameterDescription
    {
        public string Name { get; set; } = null!;
        public TelemetryItemParameterType Type { get; set; }
        public object? Value { get; set; }
    }
}
