using FirstQuad.Common.Collections;
using FirstQuad.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace FirstQuad.Telemetry
{
    public sealed class Timeline
    {
        private static readonly JsonSerializerOptions JsonSettings = new JsonSerializerOptions
        {
            WriteIndented = true,
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull
        };

        public Guid Id { get; set; }

        private readonly CycleBuffer<TimelineItem> _cycleBuffer;
        private List<TimelineItem>? _itemsToSerialize;
        private readonly Dictionary<string, decimal> _categoryCounters = new();

        private readonly int _timelineBufferSize;
        private int _timelineActualItemsCount;
        private int? _zipMaxSize;

        public DateTime StartUtc { get; set; }
        public decimal TotalTimeMs { get; set; }
        public string? TimelineName { get; set; }
        public string? TimelineParameters { get; set; }
        public List<TimelineItemParameterDescription> Parameters { get; set; } = new();
        public bool IsFinished { get; set; }

        public IEnumerable<TimelineItem>? Items => _itemsToSerialize;

        public Timeline(int bufferSize)
        {
            Id = Guid.NewGuid();
            _timelineBufferSize = bufferSize;
            StartUtc = DateTime.UtcNow;
            _cycleBuffer = new CycleBuffer<TimelineItem>(50, CycleBufferResizeStrategy.AutoExpandAndCycleRewrite, _timelineBufferSize);
        }

        public void Add(TimelineItem item)
        {
            _timelineActualItemsCount += 1;

            _cycleBuffer.Push(item);
            var category = item.Category.GetValueOrEmpty();
            if (!_categoryCounters.TryGetValue(category, out decimal categoryValue))
            {
                _categoryCounters[category] = item.EndTimeMs - item.StartTimeMs;
            }
            else
            {
                _categoryCounters[category] = categoryValue + item.EndTimeMs - item.StartTimeMs;
            }
        }

        public IDictionary<string, decimal> CategoryCounters => _categoryCounters.ToImmutableDictionary();

        public string SerializeBase64Deflate(TimelineSize timelineSize)
        {
            _zipMaxSize = 0;

            if (_cycleBuffer.HasOverflowExists)
            {
                var endOverflowTimeMs = _cycleBuffer.PeekStart.EndTimeMs;
                _cycleBuffer.Push(new TimelineItem
                {
                    Category = "Internal",
                    StartTimeMs = 0,
                    EndTimeMs = endOverflowTimeMs,
                    Text = $"Skipped due to buffer overflow (Received {_timelineActualItemsCount} items when buffer size limit is only {_timelineBufferSize}).",
                    CssColor = TelemetryConstants.SqlRequestCssColor
                });
            }

            TotalTimeMs = TelemetryManager.GetElapsedSinceOperationStarted();
            _cycleBuffer.Sort(new TimelineItemComparer());
            _itemsToSerialize = new List<TimelineItem>();

            var result = FillDataToSerializeAsync(_cycleBuffer.Length, timelineSize, 0);

            var itemsToSerialize = _cycleBuffer.Length;
            while (result.Length > timelineSize.MaxSizeTo)
            {
                var reduce = result.Length / (float)timelineSize.MaxSizeTo;
                var newItemCount = (int)(itemsToSerialize / reduce);
                if (newItemCount == 0)
                {
                    var bufferItem = _cycleBuffer[_cycleBuffer.Length - 1];
                    if (!string.IsNullOrEmpty(bufferItem.Text))
                        bufferItem.Text = bufferItem.Text.Substring(0, timelineSize.MaxSizeFrom);
                    else if (!string.IsNullOrEmpty(bufferItem.OperationName))
                        bufferItem.OperationName = bufferItem.OperationName.Substring(0, Math.Min(bufferItem.OperationName.Length, timelineSize.MaxSizeFrom));

                    return FillDataToSerializeAsync(1, timelineSize, 1);
                }

                result = FillDataToSerializeAsync(newItemCount, timelineSize, 1);
                itemsToSerialize = newItemCount;
            }

            return result;
        }

        private string FillDataToSerializeAsync(int newItemCount, TimelineSize timelineSize, int deep)
        {
            if (_itemsToSerialize == null)
            {
                throw new Exception($"Failed to fill, _itemsToSerialize is NULL");
            }

            for (var i = _cycleBuffer.Length - newItemCount; i < _cycleBuffer.Length; i++)
            {
                _itemsToSerialize.Add(_cycleBuffer[i]);
            }
            
            if (deep > 10)
            {
                _itemsToSerialize.Clear();
                
                _cycleBuffer.Push(new TimelineItem
                {
                    Category = "Internal",
                    StartTimeMs = _cycleBuffer.PeekStart.StartTimeMs,
                    EndTimeMs = _cycleBuffer.PeekEnd.EndTimeMs,
                    Text = "Timeline item above was truncated due to size overflow and unable to keep any of original items",
                    CssColor = TelemetryConstants.SummariesColor
                });
                return SerializeToBase64Gzip();
            }

            
            var result = SerializeToBase64Gzip();

            if (_zipMaxSize == null)
                _zipMaxSize = result.Length;

            if (result.Length > timelineSize.MaxSizeTo)
            {
                _itemsToSerialize.Clear();
                if (newItemCount > 1)
                {
                    var reducedCount = newItemCount / 2;
                    var skippedFromTheBeginningTimelineItems = _cycleBuffer.Length - reducedCount;

                    _itemsToSerialize.Add(new TimelineItem
                    {
                        Category = "Internal Overflow",
                        StartTimeMs = 0,
                        EndTimeMs = _cycleBuffer[skippedFromTheBeginningTimelineItems - 1].EndTimeMs,
                        Text = $"Timeline max size overflow (skipped {skippedFromTheBeginningTimelineItems} items from {_cycleBuffer.Length}) (Received {_zipMaxSize} bytes with limit of {timelineSize.MaxSizeTo} bytes).",
                        CssColor = "#ff00ff"
                    });

                    return FillDataToSerializeAsync(reducedCount, timelineSize, deep + 1);
                }

                TruncateTimelineItem(_cycleBuffer.PeekEnd);

                if (_cycleBuffer.Length > 1)
                {
                    _cycleBuffer[^2] = new TimelineItem
                    {
                        Category = "Internal",
                        StartTimeMs = _cycleBuffer.PeekStart.StartTimeMs,
                        EndTimeMs = _cycleBuffer.PeekStart.EndTimeMs,
                        Text = $"Timeline item below was truncated due to large size ({result.Length} bytes gzip with limit of {timelineSize.MaxSizeTo} bytes).",
                        CssColor = TelemetryConstants.SummariesColor
                    };
                }
                else
                {
                    _cycleBuffer.PushStart(new TimelineItem
                    {
                        Category = "Internal",
                        StartTimeMs = _cycleBuffer.PeekStart.StartTimeMs,
                        EndTimeMs = _cycleBuffer.PeekStart.EndTimeMs,
                        Text = $"Timeline item below was truncated due to large size ({result.Length} bytes gzip with limit of {timelineSize.MaxSizeTo} bytes).",
                        CssColor = TelemetryConstants.SummariesColor
                    });
                }

                return FillDataToSerializeAsync(2, timelineSize, deep + 1);
            }

            return result;
        }

        private string SerializeToBase64Gzip()
        {
            using var output = new MemoryStream();
            using var gzip = new DeflateStream(output, CompressionLevel.Optimal);
            JsonSerializer.Serialize(gzip, this, JsonSettings);
            gzip.Flush();
            return Convert.ToBase64String(output.ToArray());
        }

        private static void TruncateTimelineItem(
            TimelineItem item,
            int textSize = 1024,
            int operationNameSize = 128,
            int operationParametersSize = 128,
            int categorySize = 64,
            int exceptionMessageSize = 256)
        {
            item.Text = Truncate(item.Text, textSize);
            item.OperationName = Truncate(item.OperationName, operationNameSize);
            item.SqlParameters = Truncate(item.SqlParameters, operationParametersSize);
            item.Category = Truncate(item.Category, categorySize);
            item.ExceptionMessage = Truncate(item.ExceptionMessage, exceptionMessageSize);
        }

        private static string? Truncate(string? value, int maxSize)
        {
            int byteSize = Encoding.Unicode.GetByteCount(value ?? string.Empty);

            if (value == null || byteSize <= maxSize)
                return value;

            value = value.Substring(0, maxSize / 2);

            while (Encoding.Unicode.GetByteCount(value) > maxSize)
                value = value.Substring(0, value.Length - 1);

            return value;
        }
    }
}
