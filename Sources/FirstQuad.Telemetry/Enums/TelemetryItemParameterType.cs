﻿namespace FirstQuad.Telemetry.Enums
{
    public enum TelemetryItemParameterType
    {
        String,
        Integer,
        Boolean,
        Enum,
        Date,
        DateTime
    }
}
