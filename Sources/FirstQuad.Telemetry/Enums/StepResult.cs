﻿namespace FirstQuad.Telemetry.Enums
{
    internal enum StepResult
    {
        NextStep,
        Stop
    }
}
