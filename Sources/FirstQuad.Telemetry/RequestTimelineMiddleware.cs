using FirstQuad.Common.Helpers;
using FirstQuad.Telemetry.Classes;
using FirstQuad.Telemetry.Helpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace FirstQuad.Telemetry
{
    internal sealed class RequestTelemetryMiddleware
    {
        private readonly RequestDelegate _next;
        private Dictionary<string, byte[]>? _resources;
        private readonly IOptions<RequestTelemetryMiddlewareOptions> _options;
        private readonly ILogger<RequestTelemetryMiddleware> _logger;
        private readonly TimelineSize _timelineSize;
        internal static bool IsTimelineEnabled;

        private const string ResourcesRoot = "/rt";
        private static readonly string ClientJs =
            "!(()=>{var e=\"undefined\"!=typeof BroadcastChannel?new BroadcastChannel(\"request-timeline-channel\"):null;window.addEventListener(\"beforeunload\",(()=>{e?.close();e=null})),function(t){var n=function(){var t=this.getResponseHeader(\"x-request-timeline\");null!==t&&e?.postMessage(t)};XMLHttpRequest.prototype.send=function(e){this.addEventListener(\"load\",n),this.addEventListener(\"error\",n),t.call(this,e)}}(XMLHttpRequest.prototype.send);var{fetch:t}=window;window.fetch=async(...n)=>{let[s,o]=n,a=await t(s,o);var i=a.headers.get(\"x-request-timeline\");return null!==i&&e?.postMessage(i),a}})();";

        private static readonly byte[] ClientJsBytes = Encoding.UTF8.GetBytes($"<head><script>{ClientJs}</script>{Environment.NewLine}");
        private static readonly byte[] ContentToBeReplacedBytes = Encoding.UTF8.GetBytes("<head>");

        public static List<string> DefaultHeadersToSkip = new List<string>()
        {
            HeaderNames.Authorization,
            HeaderNames.AcceptLanguage
        };

        public RequestTelemetryMiddleware(
            RequestDelegate next,
            IOptions<RequestTelemetryMiddlewareOptions> options,
            ILoggerFactory loggerFactory,
            IConfiguration configuration,
            IWebHostEnvironment env)
        {
            _next = next;
            _options = options;
            _logger = loggerFactory.CreateLogger<RequestTelemetryMiddleware>();
            _timelineSize = new TimelineSize(_options.Value.MaxTimelineSize);
            TelemetryItemsHolder.BufferSize = _options.Value.BufferSize;
            
            var timelineConfig = configuration["ApplicationSettings:RequestTimeline"];
            IsTimelineEnabled = env.IsDevelopment() ||
                                env.IsStaging() ||
                                string.IsNullOrEmpty(timelineConfig) || timelineConfig.ToLower() == "true";
            _options.Value.IsTimelineEnabled ??= IsTimelineEnabled;

            Task.Run(() =>
            {
                _resources = ResourceHelper.ReadResources();
            });
        }

        private async Task AddRequestTimelineHeader(HttpContext context, bool isSecure, Func<string, Task<bool>>? shouldTrackHttpHeader)
        {
            if (_options.Value.IsTimelineEnabled == true && FilterTimelineByAttribute(context) && _options.Value.Filter?.Invoke(context) != false
                && isSecure && !context.Response.Headers.ContainsKey("X-Request-Timeline"))
            {
                var timeline = TelemetryManager.GetTimeline();
                timeline.TimelineName = context.Request.Path + context.Request.QueryString.Value;
                timeline.TimelineParameters = context.Request.Method;
                
                foreach(var header in context.Request.Headers)
                {
                    if ((shouldTrackHttpHeader == null || (await shouldTrackHttpHeader.Invoke(header.Key))) && !DefaultHeadersToSkip.Contains(header.Key))
                    {
                        timeline.Parameters.Add(new TimelineItemParameterDescription
                        {
                            Name = header.Key,
                            Type = Enums.TelemetryItemParameterType.String,
                            Value = header.Value.ToString()
                        });
                    }
                }
                
                var parameters = new List<TimelineItemParameterDescription>();
                if (timeline.Items != null && shouldTrackHttpHeader != null)
                {
                    foreach (var item in timeline.Items)
                    {
                        if (item.Category != "HTTP")
                            continue;

                        parameters.Clear();
                        foreach (var parameter in item.Parameters)
                        {
                            var shouldAdd = await shouldTrackHttpHeader.Invoke(parameter.Name);
                            if (shouldAdd)
                                parameters.Add(parameter);
                        }

                        item.Parameters = parameters;
                    }
                }
                
                var serializedTimeline = TelemetryManager.GetBase64SerializedTimeline(_timelineSize);
                context.Response.Headers.Add("X-Request-Timeline", serializedTimeline);
            }
        }

        private void AddClientHashToContentSecurityPolicyHeader(HttpContext context)
        {
            var cspBuilder = new ContentSecurityPolicyBuilder(context.Response.Headers.ContentSecurityPolicy);

            cspBuilder.Script.AddSha256(ClientJs);

            context.Response.Headers.ContentSecurityPolicy = cspBuilder.Build();
        }

        // ReSharper disable once UnusedMember.Global
        public async Task Invoke(HttpContext context, IWebHostEnvironment hostingEnvironment)
        {
            var response = context.Response;

            TelemetryManager.StartNewOperation();

            context.Response.OnStarting(async _ =>
            {
                var isSecure = await _options.Value.IsSecureToReturn(context);
                if (_options.Value.IsSqlTimelineHeadersEnabled && isSecure)
                {
                    if (!response.Headers.ContainsKey("X-SQL-Time-Milliseconds"))
                    {
                        response.Headers.Add(
                            "X-SQL-Time-Milliseconds",
                            TelemetryManager.GetSqlProcessingCounter().ToString(CultureInfo.InvariantCulture));
                    }
                    if (!response.Headers.ContainsKey("X-SQL-Counter"))
                    {
                        response.Headers.Add(
                         "X-SQL-Counter",
                         TelemetryManager.GetSqlOperationsCounter().ToString());
                    }
                    if (!response.Headers.ContainsKey("X-SQL-SelectRows"))
                    {
                        response.Headers.Add(
                         "X-SQL-SelectRows",
                         TelemetryManager.GetSqlSelectRowsCounter().ToString(CultureInfo.InvariantCulture));
                    }
                    if (!response.Headers.ContainsKey("X-SQL-BytesReceived"))
                    {
                        response.Headers.Add(
                        "X-SQL-BytesReceived",
                        TelemetryManager.GetSqlBytesReceivedCounter().ToString(CultureInfo.InvariantCulture));
                    }
                }
                if (_options.Value.IsProcessingTimeHeaderEnabled && isSecure && !response.Headers.ContainsKey("X-Processing-Time-Milliseconds"))
                {
                    response.Headers.Add(
                    "X-Processing-Time-Milliseconds",
                    TelemetryManager.GetElapsedSinceOperationStarted().ToString(CultureInfo.InvariantCulture));
                }
                if (_options.Value.IsEnvironmentNameHeaderEnabled && isSecure && !response.Headers.ContainsKey("X-Environment-Name"))
                {
                    response.Headers.Add(
                    "X-Environment-Name",
                    hostingEnvironment.EnvironmentName);
                }
                if (_options.Value.IsEnvironmentMachineNameHeaderEnabled && isSecure && !response.Headers.ContainsKey("X-Environment-MachineName"))
                {
                    response.Headers.Add(
                    "X-Environment-MachineName",
                    Environment.MachineName);
                }

                var request = context.Request;
                if (_options.Value.AutomaticallyAddClientHashToContentSecurityPolicy && 
                    request.Method == "GET" &&
                    !request.Path.Value!.ToLower().Equals("/rt") &&
                    _resources != null && 
                    request.Headers["accept"].ToString().Contains("text/html"))
                {
                    AddClientHashToContentSecurityPolicyHeader(context);
                }

                await AddRequestTimelineHeader(context, isSecure, _options.Value.ShouldTrackHeader);

                TelemetryManager.FinishOperation();

            }, this);

            context.Response.OnCompleted(_ =>
            {
                TelemetryManager.CleanupOperation();

                return Task.CompletedTask;
            }, this);

            try
            {
                var request = context.Request;
                if (request.Path.Value == null)
                {
                    await _next(context);
                    return;
                }

                var file = request.Path.Value!.ToLower();
                var canHandleResourceFile = request.Method == "GET" && file.StartsWith(ResourcesRoot) && !file.Equals("/rt");
                var canHandleTelemetryPage = IsTimelineEnabled && request.Method == "GET" && file.Equals("/rt");
                var canInsertBroadcastScript =  _options.Value.AutomaticallyAddBrowserClient && request.Method == "GET" && _resources != null && request.Headers["accept"].ToString().Contains("text/html");

                if (!canInsertBroadcastScript && !canHandleResourceFile && !canHandleTelemetryPage)
                {
                    await _next(context);
                }
                else if (canHandleResourceFile)
                {
                    var resourceFile = file.Substring(ResourcesRoot.Length + 1);
                    if (_resources!.ContainsKey(resourceFile))
                    {
                        var data = _resources[resourceFile];
                        var mimeType = MimeMapping.GetMimeMapping(resourceFile);
                        context.Response.StatusCode = 200;
                        context.Response.ContentType = mimeType;
                        context.Response.ContentLength = data.Length;

                        await context.Response.Body.WriteAsync(data);
                    }
                }
                else if (canHandleTelemetryPage)
                {
                    response.StatusCode = 200;
                    response.ContentType = "text/html";

                    string templateHtml;
                    if (_resources?.TryGetValue("index.html", out var indexData) == true)
                    {
                        templateHtml = Encoding.UTF8.GetString(indexData);

                        templateHtml = templateHtml.Replace("{{product_name}}", _options.Value.ProductName ?? "1stQuad Telemetry Measurement");
                        templateHtml = templateHtml.Replace("{{head_template}}", _options.Value.HeadTemplate ?? string.Empty);
                        templateHtml = templateHtml.Replace("{{body_start_template}}", _options.Value.BodyStartTemplate ?? string.Empty);
                        templateHtml = templateHtml.Replace("{{body_end_template}}", _options.Value.BodyEndTemplate ?? string.Empty);
                    }
                    else
                    {
                        templateHtml = "Request Telemetry Error: Missing HTML template in resources";
                    }

                    if (_resources != null)
                    {
                        var templateHtmlAsBytesWithBroadcastScript = InsertBrowserClientScript(Encoding.UTF8.GetBytes(templateHtml), response);
                        await response.Body.WriteAsync(templateHtmlAsBytesWithBroadcastScript);
                    }
                    else
                        await response.WriteAsync(templateHtml);
                }
                else if (canInsertBroadcastScript)
                {
                    var existingBody = response.Body;
                    using var interceptedBody = new MemoryStream();

                    // We set the response body to our stream, so we can read after the chain of middlewares have been called.
                    context.Response.Body = interceptedBody;
                    try
                    {
                        await _next(context);
                    }
                    finally
                    {
                        response.Body = existingBody;
                    }

                    if (response.StatusCode == 200 && response.ContentType.GetValueOrEmpty().Contains("text/html"))
                    {
                        var pageAsBytes = InsertBrowserClientScript(interceptedBody.GetBuffer().AsMemory(0, (int)interceptedBody.Length), response);
                        await response.Body.WriteAsync(pageAsBytes);
                    }
                    else if (interceptedBody.Length > 0)
                    {
                        await response.Body.WriteAsync(interceptedBody.GetBuffer().AsMemory(0, (int)interceptedBody.Length));
                    }
                }
            }
            catch (TaskCanceledException)
            {
                //ignore
                throw;
            }
            catch (Exception ex)
            {
                TelemetryManager.AddTimelineItem(new TimelineItem
                {
                    Text = $"{context.Request.Method} {context.Request.Path}",
                    Exception = ex,
                    CssColor = TelemetryConstants.ErrorCssColor
                });
                _logger.LogError(ex, ex.Message);

                response.Clear();
                response.ContentType = "application/json";
                response.StatusCode = (int)HttpStatusCode.InternalServerError;

                string errorJson;

                if (hostingEnvironment.IsProduction())
                {
                    errorJson = JsonSerializer.Serialize(new
                    {
                        message = "Internal Server Error",
                        statusCode = response.StatusCode
                    });
                }
                else
                {
                    errorJson = JsonSerializer.Serialize(new
                    {
                        statusCode = response.StatusCode,
                        message = ex.Message,
                        source = ex.Source,
                        stackTrace = ex.StackTrace
                    });
                }

                await response.WriteAsync(errorJson);
            }
        }
        
        private static Memory<byte> InsertBrowserClientScript(Memory<byte> pageAsBytes, HttpResponse response)
        {
            var result = pageAsBytes.ReplaceBytes(ContentToBeReplacedBytes, ClientJsBytes, response);
            return result;
        }

        private static bool FilterTimelineByAttribute(HttpContext context)
        {
            var endpoint = context.GetEndpoint();
            var descriptor = endpoint?.Metadata.GetMetadata<ControllerActionDescriptor>();
            return descriptor?.MethodInfo.GetCustomAttribute<DisableTimelineAttribute>() == null;
        }
    }
}