using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;

namespace FirstQuad.Telemetry
{
    internal class TelemetryItemsHolder
    {
        private static readonly AsyncLocal<TelemetryItemsHolder> _current = new AsyncLocal<TelemetryItemsHolder>();
        internal static int BufferSize { get; set; } = 500;

        public TelemetryItemsHolder()
        {
            Stopwatch = Stopwatch.StartNew();
            Timeline = new Timeline(BufferSize);
            SqlOperationsCounter = 0;
            TimelineItem2StartTime = new ConcurrentDictionary<string, decimal>();
        }

        public static TelemetryItemsHolder Current
        {
            get
            {
                if (_current.Value == null)
                {
                    //Debug.WriteLine($"{nameof(TelemetryManager)} detected null TelemetryItemsHolder");
                    _current.Value = new TelemetryItemsHolder();
                }
                else if (_current.Value.IsFinished)
                {
                    Debug.WriteLine($"{nameof(TelemetryManager)} detected finished TelemetryItemsHolder");
                }

                return _current.Value;
            }
            set
            {
                _current.Value = value;
            }
        }

        private bool _isFinished;
        public bool IsFinished 
        {
            get
            {
                return _isFinished;
            }

            set
            {
                _isFinished = true;
                Timeline.IsFinished = true;
            }
        }

        public Timeline Timeline { get; }

        public Stopwatch Stopwatch { get; }

        /// <summary>
        /// Made to make it possible for different custom telemetry providers to 
        /// store somewhere their start time relative to the operation
        /// </summary>
        public ConcurrentDictionary<string, decimal> TimelineItem2StartTime { get; }

        public int SqlOperationsCounter { get; set; }
        public long SqlBytesReceivedCounter { get; set; }
        public long SqlSelectRowsCounter { get; set; }
        public int SqlProcessingCounter { get; set; }
    }
}
