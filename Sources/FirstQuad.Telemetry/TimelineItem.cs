using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using FirstQuad.Telemetry.Helpers;

namespace FirstQuad.Telemetry
{
    public class TimelineItem
    {
        public TimelineItem()
        {
            StartTimeMs = TelemetryManager.GetElapsedSinceOperationStarted();
        }

        public TimelineItem(string operationName)
        {
            StartTimeMs = TelemetryManager.GetElapsedSinceOperationStarted();
            OperationName = operationName;
        }
        
        public string? Text { get; set; }
        public string? CssColor { get; set; }
        public string? OperationName { get; set; }
        public string? Comment { get; set; }
        public string? SqlParameters { get; set; }
        public List<TimelineItemParameterDescription> Parameters { get; set; } = new();

        public decimal StartTimeMs { get; set; }
        public decimal EndTimeMs { get; set; }

        public string? Category { get; set; }

        public bool IsException
        {
            get
            {
                return Exception != null;
            }
        }

        private string? _exceptionMessage;
        public string? ExceptionMessage
        {
            get
            {
                if (_exceptionMessage != null)
                {
                    return _exceptionMessage;
                }
                if (Exception != null)
                {
                    return Exception.FullMessage();
                }
                return null;
            }
            internal set
            {
                _exceptionMessage = value;
            }
        }

        [JsonIgnore]
        public Exception? Exception { get; set; }
    }
}
