﻿using System;

namespace FirstQuad.Telemetry
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class DisableTimelineAttribute : Attribute
    {
    }
}
