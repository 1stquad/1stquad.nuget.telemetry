using System.Collections.Generic;

namespace FirstQuad.Telemetry
{
    public class TimelineItemComparer : IComparer<TimelineItem>
    {
        // Call CaseInsensitiveComparer.Compare with the parameters reversed.
        public int Compare(TimelineItem? x, TimelineItem? y)
        {
            if (x == null && y == null) return 0;
            if (x == null && y != null) return -1;
            if (x != null && y == null) return 1;

            return x!.StartTimeMs.CompareTo(y!.StartTimeMs);
        }
    }
}
