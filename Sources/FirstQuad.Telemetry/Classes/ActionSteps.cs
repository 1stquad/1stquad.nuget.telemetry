﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using FirstQuad.Telemetry.Enums;

namespace FirstQuad.Telemetry.Classes
{
    internal sealed class ActionStep
    {
        public string ActionKey { get; set; } = null!;

        public Func<HttpContext, Task<StepResult>> Action { get; set; } = null!;
    }

    internal sealed class ActionSteps : List<ActionStep>
    {
        public void AddStep(string key, Func<HttpContext, Task<StepResult>> action)
        {
            Add(new ActionStep
            {
                ActionKey = key,
                Action = action
            });
        }
    }
}
