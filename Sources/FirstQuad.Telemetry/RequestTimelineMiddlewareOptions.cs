using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace FirstQuad.Telemetry
{
    public class RequestTelemetryMiddlewareOptions
    {
        public Func<HttpContext, Task<bool>> IsSecureToReturn { get; set; } = null!;
        public Func<string, Task<bool>>? ShouldTrackHeader { get; set; }
        public bool? IsTimelineEnabled { get; set; }
        public int BufferSize { get; set; } = 500;
        public Func<HttpContext, bool>? Filter { get; set; }
        public int MaxTimelineSize { get; set; } = 16 * 1024;

        public bool AutomaticallyAddBrowserClient { get; set; } = true;
        public bool AutomaticallyAddClientHashToContentSecurityPolicy { get; set; }
        public bool IsSqlTimelineHeadersEnabled {get; set;} = true;
        public bool IsEnvironmentNameHeaderEnabled {get; set;} = true;
        public bool IsEnvironmentMachineNameHeaderEnabled {get; set;} = true;
        public bool IsProcessingTimeHeaderEnabled { get; set; } = true;

        public string? ProductName { get; set; }
        public string? HeadTemplate { get; set; }
        public string? BodyStartTemplate { get; set; }
        public string? BodyEndTemplate { get; set; }
    }
}