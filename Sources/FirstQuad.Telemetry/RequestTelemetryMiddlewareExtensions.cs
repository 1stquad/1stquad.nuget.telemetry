using System.Diagnostics;
using FirstQuad.Telemetry.DiagnosticListeners;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Options;

namespace FirstQuad.Telemetry
{
    public static class RequestTelemetryMiddlewareExtensions
    {
        public static IApplicationBuilder UseRequestTelemetry(
            this IApplicationBuilder app, 
            RequestTelemetryMiddlewareOptions options)
        {
            DiagnosticListener.AllListeners.Subscribe(new HttpRequestDiagnosticsListener());
            DiagnosticListener.AllListeners.Subscribe(new SqlClientListener());
            
            return app.UseMiddleware<RequestTelemetryMiddleware>(Options.Create(options));
        }
    }
}