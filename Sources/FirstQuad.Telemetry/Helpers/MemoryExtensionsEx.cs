﻿using System;
using Microsoft.AspNetCore.Http;

namespace FirstQuad.Telemetry.Helpers;

public static class MemoryExtensionsEx
{
    internal static int FindBytes(this Span<byte> src, byte[] find)
    {
        var index = -1;
        var matchIndex = 0;
        // handle the complete source array
        for (var i = 0; i < src.Length; i++)
        {
            if (src[i] == find[matchIndex])
            {
                if (matchIndex == find.Length - 1)
                {
                    index = i - matchIndex;
                    break;
                }
                matchIndex++;
            }
            else if (src[i] == find[0])
            {
                matchIndex = 1;
            }
            else
            {
                matchIndex = 0;
            }

        }
        return index;
    }
        
    internal static Memory<byte> ReplaceBytes(this Memory<byte> srcMem, byte[] search, byte[] repl, HttpResponse response)
    {
        var src = srcMem.Span;
        var index = FindBytes(src, search);

        Memory<byte> dst;
        if (index >= 0)
        {
            dst = new byte[src.Length - search.Length + repl.Length];
            // before found array
            src.Slice(0, index).CopyTo(dst.Span);
            // repl copy
            repl.AsSpan().CopyTo(dst.Span.Slice(index));
            // rest of src array
            src.Slice(index + search.Length).CopyTo(dst.Span.Slice(index + repl.Length, src.Length - (index + search.Length)));

            response.ContentLength = dst.Length;
        }
        else
            dst = srcMem;

        return dst;
    }
}