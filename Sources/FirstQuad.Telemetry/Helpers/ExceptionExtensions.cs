using System;
using System.Linq;

namespace FirstQuad.Telemetry.Helpers;

internal static class ExceptionExtensions
{
    public static string FullMessage(this Exception ex)
    {
        if (ex is AggregateException aex)
            return aex.InnerExceptions.Aggregate("[", (total, next) => $"{total}[{next.FullMessage()}] ") + "]";

        var msg = ex.Message.Replace(", see inner exception.", "").Trim();

        var innerMsg = ex.InnerException?.FullMessage();

        if (!string.IsNullOrEmpty(innerMsg) && innerMsg != msg)
            msg = $"{msg} => [{innerMsg}]";

        return msg;
    }

}