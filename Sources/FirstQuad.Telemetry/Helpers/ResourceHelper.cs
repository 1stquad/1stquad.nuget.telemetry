﻿using System.Collections.Generic;
using System.IO.Compression;
using System.IO;
using System.Linq;

namespace FirstQuad.Telemetry.Helpers;

internal static class ResourceHelper
{
    internal static Dictionary<string, byte[]>? ReadResources()
    {
        var result = new Dictionary<string, byte[]>();

        var assembly = typeof(ResourceHelper).Assembly;
        var resourceStream = assembly.GetManifestResourceStream("FirstQuad.Telemetry.Request_Telemetry_Zip");
        
        if (resourceStream != null)
        {
            using (var archive = new ZipArchive(resourceStream, ZipArchiveMode.Read))
            {
                foreach (var entry in archive.Entries)
                {
                    using (var zipEntryStream = entry.Open())
                    {
                        using var ms = new MemoryStream();
                        zipEntryStream.CopyTo(ms);
                        result.Add(entry.FullName.ToLower().Replace("\\", "/"), ms.ToArray());
                    }
                }
            }
        }

        if (!result.Any())
            return null;

        return result;
    }
}
