using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace FirstQuad.Telemetry
{
    public sealed class IdentityEqualityComparer<T> : IEqualityComparer<T> where T : class
    {
        public int GetHashCode(T value)
        {
            // Returns identity based hash code
            return RuntimeHelpers.GetHashCode(value);
        }

        public bool Equals(T? left, T? right)
        {
            return object.ReferenceEquals(left, right);
        }
    }
}
