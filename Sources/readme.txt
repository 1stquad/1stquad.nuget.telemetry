Telemetry has been installed (Please read the instruction below)

Instruction 

To apply telemetry page and browser client script automatically you have to use line of code below:

app.UseRequestTelemetry(new RequestTelemetryMiddlewareOptions
{
    AutomaticallyAddBrowserClient = true
});

Now the Telemetry page is connected and you can get to it via your application URL + /rt : e.g https://localhost:5000/rt

BROWSER CLIENT

Client will be applied automatically if the application has at least one </body> tag. Script for client
will be inserted on the above line of code automatically. 
p.s. If your application structure doesn't imply using <body> tags at all - please insert script
below to your Main page. This script will be used to create Broadcast Channel, connect to it and
to set up AJAX and JavaScript request-response pipeline tracking:

!(()=>{var e=\"undefined\"!=typeof BroadcastChannel?new BroadcastChannel(\"request-timeline-channel\"):null;window.addEventListener(\"beforeunload\",(()=>{e?.close();e=null})),function(t){var n=function(){var t=this.getResponseHeader(\"x-request-timeline\");null!==t&&e?.postMessage(t)};XMLHttpRequest.prototype.send=function(e){this.addEventListener(\"load\",n),this.addEventListener(\"error\",n),t.call(this,e)}}(XMLHttpRequest.prototype.send);var{fetch:t}=window;window.fetch=async(...n)=>{let[s,o]=n,a=await t(s,o);var i=a.headers.get(\"x-request-timeline\");return null!==i&&e?.postMessage(i),a}})();

If inserting script process is automated, the following script will be added to your page:
<script type="text/javascript" src="~/.../main.js"></script>

TELEMETRY PAGE

As it was already mentioned, you can get access to your page via URL e.g https://localhost:5000/rt
What can you notice while using that page:

- How much time request-response pipeline has taken;
- Values of SQL Queries that were executed during the pipeline;
- Detailed information about each step (time, description, number of same steps in pipeline);
- etc.

1stQuad members can change styles manually if they need (change .scss), connect Telemetry project
inside their main one, then build output file by using BuildUI.ps1 script and then rebuild Telemetry project
so styles will be applied and will be used while displaying Telemetry page.

JOBS TRACKING

Example for Quartz.NET tracking and notifying via SignalR to tracking page for:

1) Backend:

var trackOptions = new TelemetryTrackOptions
{
  Name = JobName,
  Parameters = TriggerId,
  Category = "TotalJob",
  OnNotifyAsync = async telemetryRawData =>
  {
    await SignalRManager.SendJobTimelineAsync(telemetryRawData);
  }
};

await TelemetryManager.TrackAsync(trackOptions, async () =>
{
  await CustomLogic();
});

2) Front-end:

type JobTimelineModel = {
    jobName: string;
    triggerId: string;
    rawTimeline: string;
};

class RequestTimelineService {
    private static _instance: RequestTimelineService;
    private _bc: BroadcastChannel | null = null;

    constructor() {
        if (this.isSupported){
            this._bc = new BroadcastChannel('request-timeline-channel');
        }

        if (RequestTimelineService._instance) {
            throw new Error(`You can't initialize several instances. Use getInstance instead`);
        }
    }

    public get isSupported(){
        return typeof BroadcastChannel !== 'undefined';
    }

    public static getInstanse(): RequestTimelineService {
        if (!RequestTimelineService._instance) {
            RequestTimelineService._instance = new RequestTimelineService();
        }

        return RequestTimelineService._instance;
    }

    public init() {
        signalRService.subscribe((eventData) => {
            this.addTimeline(eventData);
        });
    }

    public addTimeline(rawData: string) {
        this._bc?.postMessage(rawData);
    }
}

export const requestTimelineService = RequestTimelineService.getInstanse();