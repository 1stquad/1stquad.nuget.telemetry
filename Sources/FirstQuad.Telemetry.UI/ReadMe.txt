Instruction 

BROADCAST CHANNEL

- Broadcast
 - boot.tsx

This .tsx file is required to create / connect to existing broadcast channel we will be using to
communicate with opened Telemetry page in our application. It helps us to share information about 
requests and responses between multiple tabs opened at the same time in browser.

.tsx file needs to be translated into .js and inserted into the Layout.cshtml page inside C# MVC 
application like following (and added probably in wwwroot folder before inserting it):

<script type="text/javascript" src="~/.../main.js"></script>

How to translate .tsx to .js and get this main.js file:
- In webpack.config.js
    Chnage 90's line of code to './Broadcast/boot'
    optional: Uncomment 175's line of code 'minimize: false' to disable minimization
- Navigate to the folder with 'package.json' file
- Launch terminal
- Use 'npm run build-prod' command to get output files

Output:
wwwroot/js/main.js - boot.tsx translated into .js (Use it in C# MVC App)
wwwroot/css/site.css - Actually nothing

TELEMETRY MEASURMENT

- Telemetry
 - boot.tsx

This .tsx file is required to create our Telemetry Page (Actually page is just a component which
we can apply to any 1stQuad application and change styles according current application needs)

.tsx file needs to be translated into .js and inserted into the .cshtml page inside C# MVC 
application that will handle our Telemetry Page component like following (and added probably 
in wwwroot folder before inserting it):

<div id="request-timeline-root"></div>
<script type="text/javascript" src="./../main.js"></script>

How to translate .tsx to .js and get this main.js file:
- In webpack.config.js
    Chnage 90's line of code to './Telemetry/boot'
    optional: Uncomment 175's line of code 'minimize: false' to disable minimization
- Navigate to the folder with 'package.json' file
- Launch terminal
- Use 'npm run build-prod' command to get output files

Output:
wwwroot/js/main.js - boot.tsx translated into .js (Use it in C# MVC App)
wwwroot/css/site.css - Styles according files in Scss folder for our Telemetry Page