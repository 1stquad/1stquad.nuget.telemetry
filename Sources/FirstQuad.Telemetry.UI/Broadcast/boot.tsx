(() => {
  //#region Broadcast Channel init
  let broadcastChannel = typeof BroadcastChannel !== 'undefined' ? new BroadcastChannel('request-timeline-channel') : null;

  window.addEventListener('beforeunload', () => {
    broadcastChannel?.close();
    broadcastChannel = null;
  });
  
  //#endregion 
  
  //#region Tracking
  
  // AJAX 
  (function(send) {
    const ajaxResponseHeaderPicker = function(this: XMLHttpRequest){
      var requestTimelineHeader = this.getResponseHeader("x-request-timeline");
        if (requestTimelineHeader !== null) {
          broadcastChannel?.postMessage(requestTimelineHeader);
        }
      };
      XMLHttpRequest.prototype.send = function(data) {
        this.addEventListener("load", ajaxResponseHeaderPicker);
        this.addEventListener("error", ajaxResponseHeaderPicker);
        send.call(this, data);
      };
    }
  )(XMLHttpRequest.prototype.send);
  
  // JavaScript Fetch API
  const { fetch: originalFetch } = window;
  window.fetch = async (...args) => {
    let [resource, config] = args;
    let response = await originalFetch(resource, config);
    var requestTimelineHeader = response.headers.get("x-request-timeline");
    if (requestTimelineHeader !== null) {
      broadcastChannel?.postMessage(requestTimelineHeader);
    }
    return response;
  };
  //#endregion
})();