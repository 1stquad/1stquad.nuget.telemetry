const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const PnpWebpackPlugin = require('pnp-webpack-plugin');
const safePostCssParser = require('postcss-safe-parser');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const WatchMissingNodeModulesPlugin = require('react-dev-utils/WatchMissingNodeModulesPlugin');
const ModuleNotFoundPlugin = require('react-dev-utils/ModuleNotFoundPlugin');
const paths = require('./Telemetry/config/paths');
const appPackageJson = require(paths.appPackageJson);

const bundleOutputDir = './build/static';

module.exports = function(webpackEnv) {
    const isDevBuild = !(webpackEnv && webpackEnv.prod);
    const isProdBuild = !isDevBuild;
    process.env.NODE_ENV = isDevBuild ? 'development' : 'production';

    const cssPath = 'css/';
    const jsPath = 'js/';
    const chunksPath = 'chunks/';
    const publicPath = '/';

    const scssConfigs = [
        isDevBuild ? 'style-loader' : MiniCssExtractPlugin.loader,
        'css-loader',
        'sass-loader'
    ];

    // ReSharper disable once UseOfImplicitGlobalInFunctionScope
    const dirName = __dirname;

    let plugins = [
        // new webpack.DllReferencePlugin({
        //     context: dirName,
        //     manifest: require('./wwwroot/vendor-manifest.json')
        // }),
        new ForkTsCheckerWebpackPlugin(),
        // This gives some necessary context to module not found errors, such as
        // the requesting resource.
        new ModuleNotFoundPlugin(paths.appPath),
        // This is necessary to emit hot updates (currently CSS only):
        isDevBuild && new webpack.HotModuleReplacementPlugin(),
        // Watcher doesn't work well if you mistype casing in a path so we use
        // a plugin that prints an error when you attempt to do this.
        // See https://github.com/facebook/create-react-app/issues/240
        isDevBuild && new CaseSensitivePathsPlugin(),
        // If you require a missing module and then `npm install` it, you still have
        // to restart the development server for Webpack to discover it. This plugin
        // makes the discovery automatic so you don't have to restart.
        // See https://github.com/facebook/create-react-app/issues/186
        new WatchMissingNodeModulesPlugin(paths.appNodeModules)
    ].filter(Boolean);

    if (isProdBuild) {
        plugins.push(new MiniCssExtractPlugin({ filename: cssPath + 'telemetry.css' }));
    }

    plugins.push(new ProgressBarPlugin());

    const aliases = {
        '@app': path.resolve(dirName, 'Telemetry/')
    };

    return [
        {
            mode: isDevBuild ? 'development' : 'production',
            // Stop compilation early in production
            bail: isProdBuild,
            stats: { modules: false },
            // These are the "entry points" to our application.
            // This means they will be the "root" imports that are included in JS bundle.
            entry: [ 
                // Include an alternative client for WebpackDevServer. A client's job is to
                // connect to WebpackDevServer by a socket and get notified about changes.
                // When you save a file, the client will either apply hot updates (in case
                // of CSS changes), or refresh the page (in case of JS changes). When you
                // make a syntax error, this client will display a syntax error overlay.
                // Note: instead of the default WebpackDevServer client, we use a custom one
                // to bring better experience for Create React App users. You can replace
                // the line below with these two lines if you prefer the stock client:
                // require.resolve('webpack-dev-server/client') + '?/',
                // require.resolve('webpack/hot/dev-server'),
                isDevBuild && require.resolve('react-dev-utils/webpackHotDevClient'),
                // Finally, this is your app's code:
                webpackEnv.bootFolderName === 'Telemetry' ? './Telemetry/boot' :
                webpackEnv.bootFolderName === 'Broadcast' ? './Broadcast/boot' : ''
                // We include the app code last so that if there is a runtime error during
                // initialization, it doesn't blow up the WebpackDevServer client, and
                // changing JS code would still trigger a refresh.
            ].filter(Boolean),
            resolve: {
                extensions: ['.js', '.jsx', '.ts', '.tsx'],
                alias: aliases,
                plugins: [
                    // Adds support for installing with Plug'n'Play, leading to faster installs and adding
                    // guards against forgotten dependencies and such.
                    PnpWebpackPlugin
                    // Prevents users from importing files from outside of src/ (or node_modules/).
                    // This often causes confusion because we only process files within src/ with babel.
                    // To fix this, we prevent you from importing files out of src/ -- if you'd like to,
                    // please link the files into your node_modules/ and let module-resolution kick in.
                    // Make sure your source files are compiled, as they will not be processed in any way.
                ]
            },
            resolveLoader: {
                plugins: [
                    // Also related to Plug'n'Play, but this time it tells Webpack to load its loaders
                    // from the current package.
                    PnpWebpackPlugin.moduleLoader(module)
                ]
            },
            output: {
                // The build folder.
                path: path.join(dirName, bundleOutputDir),            
                // Add /* filename */ comments to generated require()s in the output.
                pathinfo: isDevBuild,
                // We inferred the "public path" (such as / or /my-project) from homepage.
                // We use "/" in development.
                publicPath: publicPath,
                // There will be one main bundle, and one file per asynchronous chunk.
                filename: jsPath + '[name].js',
                // There are also additional JS chunk files if you use code splitting.
                chunkFilename: chunksPath + '[name].[contenthash:8].chunk.js',
                // Point sourcemap entries to original disk location (format as URL on Windows)
                devtoolModuleFilenameTemplate: isProdBuild
                ? info => path.relative(paths.appSrc, info.absoluteResourcePath).replace(/\\/g, '/')
                : isDevBuild && (info => path.resolve(info.absoluteResourcePath).replace(/\\/g, '/')),
                // Prevents conflicts when multiple Webpack runtimes (from different apps)
                // are used on the same page.
                jsonpFunction: `webpackJsonp${appPackageJson.name}`,
                // this defaults to 'window', but by setting it to 'this' then
                // module chunks which are built will work in web workers as well.
                globalObject: 'this'
            },
            module: {
                rules: [
                    {
                        test: /\.tsx?$/,
                        include: /Telemetry/,
                        loader: 'ts-loader',

                        exclude: /node_modules/,
                        options: {
                          // disable type checker - we will use it in fork plugin
                          transpileOnly: true
                        }
                    },
                    {
                        test: /\.(s?)css$/,
                        include: /Telemetry/,
                        use: scssConfigs
                    },
                    {
                        test: /\.(png|jpg|jpeg|gif|svg)$/,
                        use: [{ loader: 'url-loader', options: { limit: 25000 } }]
                    },
                    {
                        test: /\.(ttf|eot|svg|woff|woff2)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                        loader: 'file-loader',
                        options: {
                            publicPath: function (url) {
                                return '/fonts/' + url;
                            },
                            outputPath: 'fonts/',
                            name: '[name].[ext]'
                        }
                    }
                ]
            },
            optimization: {
                //minimize: false,
                minimizer: [
                    new TerserPlugin({
                        extractComments: false,
                        terserOptions: {
                            warnings: false,
                            compress: {
                                ecma: 8,
                                warnings: false,
                                // Disabled because of an issue with Uglify breaking seemingly valid code:
                                // https://github.com/facebook/create-react-app/issues/2376
                                // Pending further investigation:
                                // https://github.com/mishoo/UglifyJS2/issues/2011
                                comparisons: false,
                                // Disabled because of an issue with Terser breaking valid code:
                                // https://github.com/facebook/create-react-app/issues/5250
                                // Pending further investigation:
                                // https://github.com/terser-js/terser/issues/120
                                inline: 2
                            },
                            parse: {},
                            // Added for profiling in devtools
                            keep_classnames: false,
                            keep_fnames: false,
                            output: {
                                comments: false,
                                // Turned on because emoji and regex is not minified properly using default
                                // https://github.com/facebook/create-react-app/issues/2488
                                ascii_only: true
                            }
                        },
                        parallel: true,
                        cache: true,
                        sourceMap: true
                    }),
                    // This is only used in production mode
                    new OptimizeCSSAssetsPlugin({
                        cssProcessorOptions: {
                            parser: safePostCssParser,
                            map: {
                                // `inline: false` forces the sourcemap to be output into a
                                // separate file
                                inline: false,
                                // `annotation: true` appends the sourceMappingURL to the end of
                                // the css file, helping the browser find the sourcemap
                                annotation: true
                            }
                        },
                        cssProcessorPluginOptions: {
                            preset: ['default', { minifyFontValues: { removeQuotes: false } }]
                        }
                    })
                ],
            },
            plugins: plugins,
            devtool: 'source-map',
            // Some libraries import Node modules but don't use them in the browser.
            // Tell Webpack to provide empty mocks for them so importing them works.
            node: {
                module: 'empty',
                dgram: 'empty',
                dns: 'mock',
                fs: 'empty',
                http2: 'empty',
                net: 'empty',
                tls: 'empty',
                child_process: 'empty',
                global: true
            },
            // Turn off performance processing because we utilize
            // our own hints via the FileSizeReporter
            performance: false
        }
    ];
};