export type TimelineItem = {
    text?: string;
    cssColor?: string;
    operationName?: string;
    comment?: string;
    sqlParameters?: string;
    startTimeMs: number;
    endTimeMs: number;
    isException?: boolean;
    exceptionMessage?: string;
    parameters?: ItemParameterDescription[]
};

export type RequestTimelineData = {
    id: string;
    startUtc: string;
    totalTimeMs: number;
    timelineName: string;
    timelineParameters: string;
    parameters: ItemParameterDescription[];
    items: TimelineItem[];
    raw?: string;
};

export type HighlighterOptions = {
    html?: boolean;
    classPrefix?: string;
    colors?: {
        keyword: string;
        function: string;
        number: string;
        string: string;
        special: string;
        bracket: string;
    }
};

export type ItemParameterDescription = {
    name: string;
    type: ItemParameterType;
    value: string;
}

export enum ItemParameterType {
    String = "String",
    Integer = "Integer",
    Boolean = "Boolean",
    Date = "Date",
    DateTime = "DateTime"
}