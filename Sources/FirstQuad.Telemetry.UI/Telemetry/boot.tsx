import React from 'react';
import { render } from 'react-dom';
import {App} from "./App";
import './App.scss';

const root = document.getElementById('request-timeline-root');
const title = root?.getAttribute('title')
render(
    <React.StrictMode>
        <App title={title || ""} />
    </React.StrictMode>,
    root
);