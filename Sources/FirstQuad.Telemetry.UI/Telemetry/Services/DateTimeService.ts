Date.prototype.toJSON = function () {
    //use custom implementation for better performance
    const year = this.getUTCFullYear().toString();
    let month = (this.getUTCMonth() + 1).toString();
    let day = this.getUTCDate().toString();
    let hh = this.getUTCHours().toString();
    let mm = this.getUTCMinutes().toString();
    let ss = this.getUTCSeconds().toString();

    if (month.length === 1)
        month = '0' + month;
    if (day.length === 1)
        day = '0' + day;
    if (hh.length === 1)
        hh = '0' + hh;
    if (mm.length === 1)
        mm = '0' + mm;
    if (ss.length === 1)
        ss = '0' + ss;

    //'YYYY-MM-DDTHH:MM:SS.Z'
    return `${year}-${month}-${day}T${hh}:${mm}:${ss}Z`;
};

export class DateTimeService {
    static ISO_8601_date: RegExp = /^\d{4}-\d\d-\d\d(T\d\d:\d\d(:\d\d)?(\.\d+)?(([+-]\d\d:\d\d)|Z)?)?$/i;

    //#region NEW API

    static toUiClientShortDateTime(value: Date): string {
        let monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        let d = value.getDate().toString();
        let mIndex = value.getMonth();
        let m = monthNames[mIndex];
        let h = value.getHours().toString();
        let mm = value.getMinutes().toString();

        d = d.length === 1 ? ('0' + d) : d;
        m = m.length === 1 ? ('0' + m) : m;
        h = h.length === 1 ? ('0' + h) : h;
        mm = mm.length === 1 ? ('0' + mm) : mm;

        const isToday = (new Date()).toDateString() === value.toDateString();
        return (isToday ? '' : (d + ' ' + m + ' ')) + h + ':' + mm;
    }

    static fromString(value: string): Date {
        return new Date(Date.parse(value));
    }

    //#endregion
}