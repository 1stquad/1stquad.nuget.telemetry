export class ClipboardService {
    public static copyToClipboard(text: string) {
        const input = document.createElement('textarea');
        input.style.position = 'fixed';
        input.style.top = '-5px';
        input.style.border = 'none';
        input.style.outline = 'none';
        input.style.boxShadow = 'none';
        input.style.background = 'transparent';
        input.innerHTML = text;
        document.body.appendChild(input);
        input.focus();
        input.select();
        try {
            document.execCommand('copy');
        } finally {
            document.body.removeChild(input);
        }
    }
}
