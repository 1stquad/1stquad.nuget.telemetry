import * as React from 'react';
import { observer } from 'mobx-react';
import { observable, action, runInAction } from 'mobx';
import { Form, FormGroup, Input, Badge } from 'reactstrap';

import { RequestTimelineData, TimelineItem } from './Models';
import { Report } from './Components/Report/Report';
import { Highlighter } from './Components/Highlighter';
import { parseRawData } from './Components/Requests/DecodeHelper';
import { Requests } from './Components/Requests/Requests';
import { RequestTimeline } from './Components/RequestTimeline/RequestTimeline';
import { NotificationContainer } from '@app/Components/ToastNotification';
import { ToolTipContainer } from '@app/Components/ToolTipItem';
import { ClipboardService } from './Services/ClipboardService';

@observer
export class App extends React.Component<{ title: string }> {
    @observable private _base64Timeline?: string;
    @observable private _selectedEvent?: TimelineItem;
    @observable private _requestsData: RequestTimelineData[] = [];
    @observable private _showRequests: boolean = true;
    @observable private _requestFilter: string = '';
    @observable private _copied: boolean = false;
    private _bc: BroadcastChannel | null = null;

    componentDidMount() {
        this._requestFilter = localStorage.getItem('DevToolRequestFilter') || '';
        this._base64Timeline = localStorage.getItem('DevToolRequestTimeline') || undefined;
        if (typeof BroadcastChannel !== 'undefined') {
            this._bc = new BroadcastChannel('request-timeline-channel');
        }
        this._bc?.addEventListener('message', this.onBroadcastChannelMessageHandler);
    }

    componentWillUnmount() {
        this._bc?.removeEventListener('message', this.onBroadcastChannelMessageHandler);
    }

    @action.bound
    onBroadcastChannelMessageHandler(e: MessageEvent) {
        const request = this._parseRawData(e.data);
        request.raw = e.data;
        if (request.id && this._requestsData.find((x) => x.id === request.id)) {
            //ignore duplicates
            return;
        }
        this._requestsData.push(request);
    }

    private _parseRawData(rawBase64: string) {
        const tempParsedRequestTimeline = parseRawData(rawBase64);
        const getLowerCasePropAttempt = Object.getOwnPropertyDescriptor(tempParsedRequestTimeline, 'totalTimeMs');
        if (getLowerCasePropAttempt === undefined) {
            tempParsedRequestTimeline.id = Object.getOwnPropertyDescriptor(tempParsedRequestTimeline, 'Id')?.value;
            tempParsedRequestTimeline.items = Object.getOwnPropertyDescriptor(tempParsedRequestTimeline, 'Items')?.value;
            tempParsedRequestTimeline.raw = Object.getOwnPropertyDescriptor(tempParsedRequestTimeline, 'Raw')?.value;
            tempParsedRequestTimeline.startUtc = Object.getOwnPropertyDescriptor(tempParsedRequestTimeline, 'StartUtc')?.value;
            tempParsedRequestTimeline.timelineName = Object.getOwnPropertyDescriptor(tempParsedRequestTimeline, 'TimelineName')?.value;
            tempParsedRequestTimeline.timelineParameters = Object.getOwnPropertyDescriptor(tempParsedRequestTimeline, 'TimelineParameters')?.value;
            tempParsedRequestTimeline.totalTimeMs = Object.getOwnPropertyDescriptor(tempParsedRequestTimeline, 'TotalTimeMs')?.value;

            for (let i = 0; i < tempParsedRequestTimeline.items.length; i++) {
                tempParsedRequestTimeline.items[i].cssColor = Object.getOwnPropertyDescriptor(tempParsedRequestTimeline.items[i], 'CssColor')?.value;
                tempParsedRequestTimeline.items[i].endTimeMs = Object.getOwnPropertyDescriptor(tempParsedRequestTimeline.items[i], 'EndTimeMs')?.value;
                tempParsedRequestTimeline.items[i].exceptionMessage = Object.getOwnPropertyDescriptor(tempParsedRequestTimeline.items[i], 'ExceptionMessage')?.value;
                tempParsedRequestTimeline.items[i].isException = Object.getOwnPropertyDescriptor(tempParsedRequestTimeline.items[i], 'IsException')?.value;
                tempParsedRequestTimeline.items[i].operationName = Object.getOwnPropertyDescriptor(tempParsedRequestTimeline.items[i], 'OperationName')?.value;
                tempParsedRequestTimeline.items[i].operationParameters = Object.getOwnPropertyDescriptor(tempParsedRequestTimeline.items[i], 'OperationParameters')?.value;
                tempParsedRequestTimeline.items[i].startTimeMs = Object.getOwnPropertyDescriptor(tempParsedRequestTimeline.items[i], 'StartTimeMs')?.value;
                tempParsedRequestTimeline.items[i].text = Object.getOwnPropertyDescriptor(tempParsedRequestTimeline.items[i], 'Text')?.value;
            }
        }

        return tempParsedRequestTimeline;
    }

    private _toggleRequests() {
        this._showRequests = !this._showRequests;
    }

    @action
    private _setFilter(value: string) {
        this._requestFilter = value;
        localStorage.setItem('DevToolRequestFilter', value);
    }

    public render() {
        let data = null;
        let parseDataError = null;

        try {
            data = this._base64Timeline ? (this._parseRawData(this._base64Timeline) as RequestTimelineData) : null;
        } catch (e) {
            parseDataError = <span className="request-decode-error">{(e as Error).toString()}</span>;
        }

        const showBadge = this._showRequests && this._requestsData && this._requestsData.length > 0;
        const infoCount = this._requestsData ? this._requestsData.length : '';
        const filter = this._showRequests ? (
            <>
                <div className="requests-filter">
                    <input className="form-control" value={this._requestFilter} onChange={(e) => this._setFilter(e.target.value)} placeholder="Filter" />
                </div>
            </>
        ) : null;
        const elements = (
            <div className="request-navi">
                <span className="request-type" onClick={() => this._toggleRequests()}>
                    <img
                        className="request-type-icon"
                        hidden={!this._showRequests}
                        title="Switch to list"
                        alt=""
                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAACXBIWXMAAAsTAAALEwEAmpwYAAACSElEQVR4nO2aPWsUURSGbzTamCIfSj4QUpvOQlJENNaxtLfzFySmlbiFnW2SImB+gNhYrqArSKwEFcEIioKdCFFwxcRHjr4LN+Nkd2YzO3M33AeGmXnn7J3z7p25c2fPOheJRLoCuArUge9UzzflspDXRI1wuZ2nJ4wmsAhMuooBJoEl5USmngEeKXjRBQZwU7nVswTb9WhMuMAAJpTbTpbgv5SU2BbwLKE1bDl0fiUb+e9cdDh/NFI0unTy0DgqRp4EaaTTuYj3SILYIxmJN7uIN3svOUqjViPlgdfou7lWNxCNBAaxRxIAx4BxW7t+7BH+vWZuAj8UYut7WV6JgfWiBhEOYwQYBt7p0E/gs9bGth1v0941f05etZGa5MfAqLRRe1eQXjugrWnga0hGXkueS+gXpb9MaWcQeKrj90MxsivZkhsBzgNntH8nrUfsF0F95qN6LwgjeN/sb23vAQ+BsyltzMu8LZdyJdCBzO2kBbIfG61eeaPXe+C0FzsGfNKxW7kTKMmI/aA2Ls2eJc+l3/ViH0izgeB4qEYuJPRZ6R+0P+DF2kh3w1ta2Pblqoz8kjyU0IekN1OMtGOjKiPbkucT+hXpbz0jawcsLWz7elVGViS/Ac5Jm9G+sVJYAj02YpfQC+9b/eJtm36qsAR6acSbb615NUVbr7abZ3WVQIFGdhSbWnIDTqoUdsKVDDCVp9Bj1VNjyQUGsJyn9Lag4KZqdlOlZNm5J5a9V4dsZWpvwte/5elEz9S94mj//WEgEom4Fn8AKKMldHJ7PHUAAAAASUVORK5CYII="
                    />
                    <img className="request-type-icon" hidden={this._showRequests} alt="Switch to base64 input" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAQUlEQVR4nO3VoREAMAzEsN9/6ZSW9oLSSBsYOQGe1BBZEwIAHTVE1oQAQEcNkTUhANBRQ2RNCAB0fDOy+iUEyO0AFrP6TF6TiQ0AAAAASUVORK5CYII=" />
                </span>
                {showBadge && (
                    <div className="request-info">
                        <Badge color="info" title="Events number">
                            {infoCount}
                        </Badge>
                        <Badge color="danger" className="requests-clear" title="Clear list" onClick={(e) => this._clearRequests(e)}>
                            Clear
                        </Badge>
                    </div>
                )}
            </div>
        );
        return (
            <Report title={this.props.title} filter={filter} headerElement={elements}>
                <NotificationContainer />
                <ToolTipContainer />
                <Form className="requests-picker" onSubmit={(e) => e.preventDefault()}>
                    <FormGroup>
                        {this._showRequests && <Requests requestFilter={this._requestFilter} requestsData={this._requestsData} base64Timeline={this._base64Timeline} onRequestClick={this._setNewTimelineRaw} />}
                        {!this._showRequests && <Input type="textarea" name="text" value={this._base64Timeline} onChange={(event) => this._setNewTimelineRaw(event.target.value)} />}
                    </FormGroup>
                </Form>
                {data && <RequestTimeline selectedEvent={this._selectedEvent} requestTimelineData={data} onSelectEvent={this.selectEvent} />}
                {parseDataError}
                {this._selectedEvent && <div className="event-details">{this._renderSelectedEvent()}</div>}
            </Report>
        );
    }

    @action.bound
    private _setNewTimelineRaw(rawData: string) {
        this._base64Timeline = rawData;
        localStorage.setItem('DevToolRequestTimeline', rawData);
    }

    private _renderSelectedEvent() {
        const event = this._selectedEvent;

        if (!event) return null;

        const showParams = (event.parameters?.length ?? 0) > 0;
        const params = showParams ? (
            event.parameters?.filter(x => x.name && x.value).map((x) => (
                <tr>
                    <td>{x.name}</td>
                    <td style={{overflowWrap: 'anywhere'}}>{x.value}</td>
                </tr>
            ))
        ) : (
            <></>
        );
        const paramsTable = showParams ? (
            <table style={{width: '100%', marginTop: '12px'}}>
                <tr>
                    <th style={{width: '20%'}}>Param</th>
                    <th>Value</th>
                </tr>
                {params}
            </table>
        ) : (
            <></>
        );

        const highlighter = new Highlighter({ html: true });

        const comment = highlighter.highlight(event.comment || '').replace(/(?:\r\n|\r|\n)/g, '<br>');

        const sqlParameters = highlighter.highlight(event.sqlParameters || '').replace(/(?:\r\n|\r|\n)/g, '<br>');
        const text = highlighter.highlight(event.text || '').replace(/(?:\r\n|\r|\n)/g, '<br>');

        const commentContainer = <div dangerouslySetInnerHTML={{ __html: comment }} />;

        const onCopyText = () => {
            ClipboardService.copyToClipboard((event.sqlParameters ?? '') + '\n' + event.text);
            runInAction(() => {
                this._copied = true;
            });

            setTimeout(() => {
                runInAction(() => {
                    this._copied = false;
                });
            }, 1000);
        };

        const sqlTextContainer = (
            <div style={{ border: 'solid 1px gray', borderRadius: '4px' }}>
                <div style={{ display: 'flex', flexDirection: 'row-reverse' }}>
                    <img
                        style={{ width: '26px', height: '26px', margin: '4px', cursor: 'pointer' }}
                        title="Copy"
                        alt="Copy"
                        src={
                            !this._copied
                                ? 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAACXBIWXMAAAsTAAALEwEAmpwYAAAA/klEQVR4nO3XUQrCQAwE0FxipXhRfz1jEYunGb+EUlLb3U6ypczA4l/JM9NFzbgB6UxmNpBn6wJBb8xvCMYz0BPDhIyzz0Kar3oIxjPKDJO+GSbEemIeZEhXzDNgq2XPO8O6Lt+kbwsrW93EMO/+j5ndgyCbNWPf/Ucx2JhnFcOETISXEjvmcWvGhAwEDBrOGHH338zsdaBmaDx0CANTk1BIJgbRkCwMMiAZGGRBojHIhERiEAGp/W12CQjOCKmNIE60EUZULSeqFiOqlhNVixFVy4mqxYiq5UTVYkTVcqJqRVSr9a8yzraRy0AogSDt0Ub+RdU6a7XQ4Qhii3wBuJ0AhigWdm0AAAAASUVORK5CYII='
                                : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAACXBIWXMAAAsTAAALEwEAmpwYAAAB40lEQVR4nO3ZTYhPURjH8c/MeBcRCUUxJSkveclmorCwsJEiZWOHDVaabNhhZ0lKzZKVZKMoC2QhKWKBkpe85P01htGtn5qkZmj+M//D/e7uuec+5/ndc8/zPOdcampqampq/g8WYbHCmYX3eK1gWnAWPehSMFsj4jmmKJSpeBEhmxXMyYg4o2DWRcQbzFAo4/EgQrYrmKMRcQmtCmUlvuMz5imUkbiV2ehUMAcj4jqGK5SF+IJuLFUow3A1s3FIwXRGxD2MVShz8DGRao2CK9tzmY0qdxTLtoh4jIkKZTpeRcj6Rg40s8Fv6VREVBVuwxiHT3iI+Q2wvykiXmbP0TBacTqDVdO/YgBtT8KT2K52f4OSpI5lwKqA2zhAdrti83yi1qDQgn0ZuHsA9garki8+oN0QsBPfIujAX9oYg7uxsdsQsiVFXeXI8Xx6f8LhPHsFbYaY1Xgbh6rwObqfzy3Pp/k1J4ZNwTI8i5jLmNxH/xG4kf77NRntuBPnbvZxyvEzWNzGKE3INFyLk4+w4Dd95iZ0V4GiQxMzARd6ZemOX5LqxdyrFnoRBwYneiXODWnflbb7KXmKoA1H4ngVmfbiXa7XKpA9cb7nX/gNsCOL+2k/QnPTswSzh9qJmpqaGk3LD+G2d+p9GigPAAAAAElFTkSuQmCC'
                        }
                        onClick={onCopyText}
                    />
                </div>
                <div dangerouslySetInnerHTML={{ __html: sqlParameters + '<br />' + text }}></div>
            </div>
        );

        return (
            <>
                {commentContainer}
                {sqlTextContainer}
                {paramsTable}
            </>
        );
    }

    @action.bound
    private selectEvent(event: TimelineItem) {
        this._selectedEvent = event;
    }

    @action
    private _clearRequests(e: React.MouseEvent) {
        this._requestsData = [];
        e.stopPropagation();
    }
}
