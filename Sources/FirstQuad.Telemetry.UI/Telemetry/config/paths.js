'use strict';

const path = require('path');
const fs = require('fs');

// Make sure any symlinks in the project folder are resolved:
// https://github.com/facebook/create-react-app/issues/637
const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = relativePath => path.resolve(appDirectory, relativePath);

const moduleFileExtensions = [
    'tsx',
    'js',
    'ts',
    'json'
];

// Resolve file paths in the same order as webpack
const resolveModule = (resolveFn, filePath) => {
    const extension = moduleFileExtensions.find(extension => {
        const fileName = resolveFn(`${filePath}.${extension}`);
        const exists = fs.existsSync(fileName);
        return exists;
    });

    if (extension) {
        return resolveFn(`${filePath}.${extension}`);
    }

    return resolveFn(`${filePath}.js`);
};

// config after eject: we're in ./config/
module.exports = {
    dotenv: resolveApp('.env'),
    appBuild: resolveApp('wwwroot'),
    appPath: resolveApp('.'),
    appIndexJs: resolveModule(resolveApp, 'ClientApp/Pages/DevTools/RequestTimeline'),
    appPublic: resolveApp('wwwroot'),
    appPackageJson: resolveApp('package.json'),
    appSrc: resolveApp('Telemetry'),
    appTsConfig: resolveApp('tsconfig.json'),
    appNodeModules: resolveApp('node_modules')
};

module.exports.moduleFileExtensions = moduleFileExtensions;
