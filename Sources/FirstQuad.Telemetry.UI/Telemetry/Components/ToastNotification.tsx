import * as React from 'react';
import { ToastContainer, ToastMessageAnimated } from 'react-toastr';

const container: React.RefObject<ToastContainer & { handleOnToastClick: Function }> = React.createRef();
type ToastContainerOptions = {
    handleOnClick?: Function;
    closeButton?: boolean;
    timeOut?: number;
};

export class NotificationContainer extends React.Component {
    render() {
        return (
            <div>
                <ToastContainer ref={container} toastMessageFactory={this._toastMessageFactory} className="toast-bottom-right" />
            </div>
        );
    }

    private _toastMessageFactory(props: object & { handleOnClick?: Function }) {
        const { handleOnClick, ...rest } = props;
        return <ToastMessageAnimated {...rest} />;
    }
}

export class NotificationHandler {

    public static suppressError(message: string){
        const excludeErrors: {pattern: string, reason: string}[] = [{
            pattern: '__gCrWeb',
            //https://stackoverflow.com/questions/26483541/referenceerror-cant-find-variable-gcrweb
            reason: 'Edge on iOS bug'
        }, {
            pattern: 'instantSearchSDKJSBridgeClearHighlight',
            //https://stackoverflow.com/questions/69261499/what-is-instantsearchsdkjsbridgeclearhighlight
            reason: 'Edge on iOS bug'
        }];

        for(const p of excludeErrors){
            if (message && message.indexOf(p.pattern) !== -1){
                //do not report
                return true;
            }
        }

        return false;
    }

    public static showError(message: React.ReactNode, title: React.ReactNode = 'Error') {
        if (typeof message === 'string' && this.suppressError(message)){
            return;
        }

        const root = container.current;
        if (root) {
            root.error(message, title, { closeButton: true });
        }
    }

    public static showInfo(message: string, title: string = 'Info') {
        const root = container.current;
        if (root) {
            root.info(message, title, { closeButton: true });
        }
    }

    public static showWarning(message: string, title: string = 'Warning') {
        const root = container.current;
        if (root) {
            root.warning(message, title, { closeButton: true });
        }
    }

    public static showSuccess(message: string, title: string = 'Success', optionsOverride?: ToastContainerOptions) {
        const root = container.current;
        if (root) {
            let options: ToastContainerOptions = { closeButton: true };
            if (optionsOverride) {
                options = Object.assign({ closeButton: true }, optionsOverride);
            }
            root.success(message, title, options);
        }
    }

    public static clear() {
        const root = container.current;
        if (root) {
            root.clear();
        }
    }
}
