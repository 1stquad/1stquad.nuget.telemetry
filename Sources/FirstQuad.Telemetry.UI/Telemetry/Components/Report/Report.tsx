import * as React from 'react';
import { observable } from 'mobx';
import { observer } from 'mobx-react';

import DocumentTitle from 'react-document-title';

type ReportProps = {
    title?: string;
    filter?:JSX.Element | null
    classNames?: {
        content?: string;
        header?: string;
        body?: string;
    }
    selfContent?: boolean;
    headerElement?: JSX.Element | null;
    isFlexContainer?: boolean;
};

@observer
export class Report extends React.Component<ReportProps> {
    @observable private _hasError!: boolean;
    @observable private _error!: Error | null;
    @observable private _componentStack!: string | null;

    componentDidCatch(error: Error, errorInfo: React.ErrorInfo) {
        this._hasError = true;
        this._error = error;
        this._componentStack = errorInfo.componentStack;
    }

    render() {
        const { title, classNames, isFlexContainer,
            selfContent, headerElement, filter } = this.props;
        if (selfContent && !this._hasError) {
            return this.props.children;
        } else {
            const contentClass = `content ${isFlexContainer ? 'content-flex' : ''} ${classNames?.content || ''}`;
            const contentBodyClass = `content-body ${classNames?.body || ''}`;
            return (
                <>
                    <nav className="navbar bg-dark border-bottom border-bottom-dark">
                        <div className="container-fluid">
                            <a className="navbar-brand" href="/">{title}</a>
                            {headerElement}
                            {filter}
                        </div>
                    </nav>
                    <div className={contentClass}>
                        {title && <DocumentTitle title={title} />}
                        <div className={contentBodyClass}>
                            {this._hasError && this.renderError()}
                            {!this._hasError && this.props.children}
                        </div>
                    </div>
                </>
            );
        }
    }

    renderError() {
        const message: string = this._error ? this._error.message : 'Error';
        return (
            <div className="error-container">
                <h3>Something went wrong</h3>
                <p>Please contact support.</p>
                <div className="stack-block">
                    <h3>{message}</h3>
                    The above error occurred in:
                    {this._componentStack}
                </div>
            </div>
        );
    }
}
