import pako from 'pako';

export function parseRawData(rawBase64: string) {
    const raw = base64ToArrayBuffer(rawBase64);
    const jsonData = pako.inflate(raw, { to: 'string', raw: true });

    return JSON.parse(jsonData);
}

function base64ToArrayBuffer(base64: string) {
    const binaryString = window.atob(base64);
    const len = binaryString.length;
    const bytes = new Uint8Array(len);
    for (let i = 0; i < len; i++) {
        bytes[i] = binaryString.charCodeAt(i);
    }
    return bytes;
}
