import * as React from 'react';
import { observer } from 'mobx-react';

import {Request} from './Request/Request';
import {RequestTimelineData} from "@app/Models";

type FilterPart = {
    type: string;
    value: string;
};

type RequestsProps = {
    requestsData: RequestTimelineData[] | null;
    base64Timeline?: string;
    requestFilter: string;
    onRequestClick: (raw: string) => void;
};

@observer
export class Requests extends React.Component<RequestsProps, {}> {

    public render() {
        const { onRequestClick, base64Timeline } = this.props;
        const filteredRequests = this._filterRequests();

        return (
            <div className="request-list">
                {filteredRequests.map((req, i) => (
                    <Request key={i} request={req} selected={req.raw === base64Timeline} onRequestClick={onRequestClick} />
                ))}
            </div>
        );
    }

    private _filterRequests(): RequestTimelineData[] {
        const { requestsData, requestFilter } = this.props;
        const filteredRequests = [];
        const filterGroups: FilterPart[][] = [];
        const filterGroupParts = requestFilter.split(';');

        for (let j = 0; j < filterGroupParts.length; j++) {
            const filterParts = filterGroupParts[j].split(' ');
            const filters: FilterPart[] = [];
            for (let i = 0; i < filterParts.length; i++) {
                let value = filterParts[i].toLowerCase();

                let type = '+URL';
                if (value.startsWith('-')) {
                    type = '-URL';
                    value = value.substring(1);
                }
                if (value.startsWith('$')) {
                    type = '+SQL';
                    value = value.substring(1);
                }

                if (!value) continue;
                filters.push({
                    value: value,
                    type: type
                });
            }
            filterGroups.push(filters);
        }

        if (requestsData) {
            for (let i = 0; i < requestsData.length; i++) {
                const request = requestsData[i];

                let matchFilters = !filterGroupParts.length;

                for (let k = 0; k < filterGroupParts.length; k++) {
                    let matchFilter = true;
                    const filters = filterGroups[k];
                    for (let j = 0; j < filters.length; j++) {
                        const filter = filters[j];
                        if (filter.type === '+URL') {
                            matchFilter = matchFilter && (request.timelineParameters + ' ' + request.timelineName).toLowerCase().includes(filter.value);
                        }
                        if (filter.type === '-URL') {
                            matchFilter = matchFilter && !(request.timelineParameters + ' ' + request.timelineName).toLowerCase().includes(filter.value);
                        }
                        if (filter.type === '+SQL') {
                            matchFilter =
                                matchFilter &&
                                request.items.map((x) => x.operationName || x.text || '')
                                    .join(' ')
                                    .toLowerCase()
                                    .includes(filter.value);
                        }
                    }
                    if (matchFilter) {
                        matchFilters = true;
                    }
                }

                if (!matchFilters) continue;

                filteredRequests.push(request);
            }
        }
        return filteredRequests;
    }
}
