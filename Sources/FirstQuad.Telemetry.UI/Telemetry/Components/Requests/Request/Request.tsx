import * as React from 'react';
import {parseRawData} from "@app/Components/Requests/DecodeHelper";
import {RequestTimelineData} from "@app/Models";
import {ClipboardService} from "@app/Services/ClipboardService";
import {NotificationHandler} from "@app/Components/ToastNotification";

export type RequestProps = {
    selected: boolean;
    request: RequestTimelineData;
    onRequestClick: (raw: string) => void;
};

export class Request extends React.PureComponent<RequestProps, {}> {
    private _copyToClipboard = (isJSON?: boolean) => () => {
        const row = this.props.request.raw || '';
        try {
            if (isJSON) {
                let json = null;
                try {
                    json = parseRawData(row);
                } catch (e) {
                    NotificationHandler.showError(`Oops, unable to parse: ${(e as Error).toString()}`);
                }
                if (json) {
                    ClipboardService.copyToClipboard(JSON.stringify(json));
                    NotificationHandler.showSuccess(`Request string copied!`, 'Copy request');
                }
            } else {
                ClipboardService.copyToClipboard(row);
                NotificationHandler.showSuccess(`Request string copied!`, 'Copy request');
            }
        } catch (err) {
            NotificationHandler.showError(`Oops, unable to copy: ${err}`);
        }
    }

    public render() {
        const { request:{totalTimeMs, items, startUtc, timelineParameters, timelineName, raw}, selected, onRequestClick } = this.props;

        let requestTimeCss = 'request-time badge';

        if (totalTimeMs > 200) {
            requestTimeCss += ' bg-danger';
        } else if (totalTimeMs > 100) {
            requestTimeCss += ' bg-warning';
        } else if (totalTimeMs > 50) {
            requestTimeCss += ' bg-info';
        } else {
            requestTimeCss += ' bg-success';
        }

        let requestCss = 'request-item';

        if (selected) {
            requestCss += ' selected';
        }

        if (items.find((x) => !!x.isException)) {
            requestCss += ' with-exception';
        }

        return (
            <div key={startUtc + timelineParameters + timelineName} className={requestCss} onClick={() => onRequestClick(raw || '')}>
                <span className="request-item-name">
                    <b>{timelineParameters}</b>
                    {timelineName}
                </span>
                <span title="Copy JSON" onClick={this._copyToClipboard(true)} className="request-item-icon icon-json" />
                <span title="Copy Base64" onClick={this._copyToClipboard()} className="request-item-icon icon-base" />
                <div className={requestTimeCss}>{totalTimeMs.toFixed(1) + 'ms'}</div>
            </div>
        );
    }
}
