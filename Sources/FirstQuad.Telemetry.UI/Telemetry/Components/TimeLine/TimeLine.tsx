import * as React from 'react';
import { TimeBlockProps } from './TimeBlock/TimeBlock';
import {TimelineHeader} from "./TimelineHeader/TimelineHeader";

type TimelineProps = {
    timeBlocks: TimeBlockProps[];
    className?: string;
};

export class Timeline extends React.PureComponent<TimelineProps, {}> {
    //https://codepen.io/MilanMilosev/pen/ONNQJM

    public render() {
        const { timeBlocks, className } = this.props;
        const cls = className ? `timeline ${className}` : 'timeline';

        return (
            <div className={cls}>
                <div id="life">
                    <TimelineHeader timeBlocks={timeBlocks} />
                    {this.props.children}
                </div>
            </div>
        );
    }
}
