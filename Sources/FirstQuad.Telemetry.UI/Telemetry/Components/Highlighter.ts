type HighlighterOptions = {
    html?: boolean;
    classPrefix?: string;
    colors?: {
        keyword: string;
        function: string;
        number: string;
        string: string;
        special: string;
        bracket: string;
    };
};

const defaultOptions: HighlighterOptions = {
    html: false,
    classPrefix: 'sql-hl-',
    colors: {
        keyword: '\x1b[35m',
        function: '\x1b[31m',
        number: '\x1b[32m',
        string: '\x1b[32m',
        special: '\x1b[33m',
        bracket: '\x1b[33m'
    }
};

const keywordsUpper: string[] = [
    'PRAGMA',
    'CREATE',
    'EXISTS',
    'INTEGER',
    'PRIMARY',
    'DECLARE',
    'letCHAR',
    'DATETIME',
    'NULL',
    'REFERENCES',
    'AND',
    'AS',
    'ASC',
    'INDEX_LIST',
    'BETWEEN',
    'BY',
    'CASE',
    'CURRENT_DATE',
    'CURRENT_TIME',
    'DELETE',
    'DESC',
    'DISTINCT',
    'EACH',
    'ELSE',
    'ELSEIF',
    'FALSE',
    'FOR',
    'FROM',
    'GROUP',
    'HAVING',
    'IF',
    'IN',
    'INSERT',
    'INTERVAL',
    'INTO',
    'IS',
    'JOIN',
    'KEY',
    'KEYS',
    'LEFT',
    'LIKE',
    'LIMIT',
    'MATCH',
    'NOT',
    'ON',
    'OPTION',
    'OR',
    'ORDER',
    'OUT',
    'OUTER',
    'REPLACE',
    'TINYINT',
    'RIGHT',
    'SELECT',
    'SET',
    'TABLE',
    'THEN',
    'TO',
    'TRUE',
    'UPDATE',
    'VALUES',
    'WHEN',
    'WHERE',
    'UNSIGNED',
    'CASCADE',
    'UNIQUE',
    'DEFAULT',
    'ENGINE',
    'TEXT',
    'auto_increment',
    'SHOW',
    'INDEX',
    'CROSS',
    'COALESCE',
    'END',
    'INNER',
    'TOP',
    'OFFSET',
    'ROWS',
    'FETCH',
    'NEXT',
    'ROWS',
    'ONLY',
    'CAST',
    'TOP',
    'EXEC'
];

const keywordsLower = keywordsUpper.map((value) => value.toLowerCase());
const keywords = keywordsUpper.concat(keywordsLower);
const clearStyle = '\x1b[0m';

export class Highlighter {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    private options: any;
    private unicodePattern: string;
    private htmlPattern: string;

    constructor(_options: HighlighterOptions) {
        this.options = Object.assign({}, defaultOptions, _options);

        this.unicodePattern = `{0}$1${clearStyle}`;
        this.htmlPattern = `<span class="${this.options.classPrefix}{0}">$1</span>`;
    }

    highlight(text: string) {
        let newText = text;

        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const rules: any = {
            special: /(=|%|\/|\*|-|,|;|:|\+|<|>)/g,
            function: {
                match: /(\w*?)\(/g,
                pattern: '{0}('
            },
            number: /(\d+)/g,
            string: /(['`].*?['`])/g,
            bracket: /([()])/g
        };

        for (const key in rules) {
            const rule = rules[key];
            let match = rule;
            let pattern = '{0}';

            if (typeof rule === 'function') {
                match = rule.match;
                pattern = rule.pattern;
            }

            let replacer: string;

            if (!this.options.html) {
                replacer = this.unicodePattern.replace('{0}', this.options.colors[key]);
            } else {
                replacer = this.htmlPattern.replace('{0}', key);
            }
            newText = newText.replace(match, pattern.replace('{0}', replacer));
        }

        const replacer = !this.options.html ? this.unicodePattern.replace('{0}', this.options.colors.keyword) : this.htmlPattern.replace('{0}', 'keyword');

        // Keywords
        for (let i = 0; i < keywords.length; i++) {
            const regEx = new RegExp(`\\b(${keywords[i]})\\b`, 'g');
            newText = newText.replace(regEx, replacer);
        }

        return newText;
    }
}
