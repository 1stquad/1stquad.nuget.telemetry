import * as React from 'react';
import {Timeline, TimelineEvent} from "../TimeLine";
import {RequestTimelineEvents} from "./RequestTimelineEvents";
import {RequestTimelineData, TimelineItem} from "@app/Models";
import {DateTimeService} from "@app/Services/DateTimeService";

type RequestTimelineProps = {
    requestTimelineData: RequestTimelineData;
    selectedEvent?: TimelineItem;
    onSelectEvent: (selectedEvent: TimelineItem) => void;
};

export class RequestTimeline extends React.PureComponent<RequestTimelineProps, {}> {
    public render() {
        const { onSelectEvent, requestTimelineData } = this.props;

        const { totalTimeMs, startUtc, timelineParameters, timelineName, parameters } = requestTimelineData;
        const timeRanges = [10, 20, 50, 100, 200, 350, 500, 1000, 2000, 5000, 10000, 30000, 60000];
        const timeSplits = [10, 10, 10, 10, 10, 14, 10, 10, 20, 10, 10, 30, 20];

        let maxTime = 0;
        let splits = 0;

        for (let i = 0; i < timeRanges.length; i++) {
            maxTime = timeRanges[i];
            splits = timeSplits[i];

            if (timeRanges[i] * 0.7 > totalTimeMs) {
                break;
            }
        }

        const msLength = 100 * (1 / maxTime);
        const headerTimeBlocks = this._getHeaderTimeBlocks(splits, maxTime, msLength);
        const events = this._getEvents(msLength);
        const width = (totalTimeMs * msLength).toFixed(2).toString() + '%';
        const dateTimeStartUtc = DateTimeService.fromString(startUtc);

        //https://codepen.io/MilanMilosev/pen/ONNQJM
        return (
            <Timeline timeBlocks={headerTimeBlocks} className="request-timeline">
                <RequestTimelineEvents events={events} onEventClick={onSelectEvent}>
                    <TimelineEvent key={'request'} onClick={() => {
                        onSelectEvent({
                            text: timelineParameters + ' ' + timelineName,
                            startTimeMs: 0,
                            endTimeMs: totalTimeMs,
                            parameters: parameters
                        })
                    }}>
                        <div className="event-pre">
                            <b>{timelineParameters}</b>
                            {timelineName}
                        </div>
                        <div className="time" style={{ width }} />
                        <b>{totalTimeMs.toFixed(1)}ms</b>
                        {DateTimeService.toUiClientShortDateTime(dateTimeStartUtc)}
                    </TimelineEvent>
                </RequestTimelineEvents>
            </Timeline>
        );
    }

    private _getHeaderTimeBlocks(splits: number, maxTime: number, msLength: number) {
        const headerTimeBlocks = [];
        const splitValue = maxTime / splits;

        for (let i = 0; i < splits; i++) {
            const ms = splitValue * i;
            const left = (ms * msLength).toFixed(2).toString() + '%';
            const title = ms > 1000 ? (ms / 1000).toFixed(1) + 's' : ms.toFixed(1);
            headerTimeBlocks.push({ left, title });
        }
        return headerTimeBlocks;
    }

    private _getEvents(msLength: number) {
        const { requestTimelineData: data, selectedEvent } = this.props;

        const events = [];
        const repeatCounts = new Map<string, number>();
        const repeatFullTime = new Map<string, number>();

        for (let i = 0; i < data.items.length; i++) {
            const { text, operationName, endTimeMs, startTimeMs } = data.items[i];
            const eventKey = text || operationName;

            if (!eventKey) continue;

            let fullTimeMs = repeatFullTime.get(eventKey) || 0;
            fullTimeMs += endTimeMs - startTimeMs;
            repeatFullTime.set(eventKey, fullTimeMs);

            let count = repeatCounts.get(eventKey) || 0;
            count++;
            repeatCounts.set(eventKey, count);
        }

        for (let i = 0; i < data.items.length; i++) {
            const { startTimeMs, endTimeMs, cssColor, text, operationName, isException, exceptionMessage } = data.items[i];
            const item = data.items[i];

            const left = (startTimeMs * msLength).toFixed(2).toString() + '%';
            const width = ((endTimeMs - startTimeMs) * msLength).toFixed(2).toString() + '%';
            const backgroundColor = cssColor ? cssColor : void 0;

            const eventKey = text || operationName || '';
            const count = eventKey ? repeatCounts.get(eventKey) || 0 : 0;
            const time = (endTimeMs - startTimeMs).toFixed(1);
            const fullTimeMs = (eventKey ? repeatFullTime.get(eventKey) || 0 : 0).toFixed(1);
            const isSelected = !!(selectedEvent && (selectedEvent.operationName || selectedEvent.text) === (operationName || text));

            events.push({ width, left, backgroundColor, count, time, fullTimeMs, text: eventKey, item, isException: !!isException, isSelected, exceptionMessage });
        }
        return events;
    }
}
