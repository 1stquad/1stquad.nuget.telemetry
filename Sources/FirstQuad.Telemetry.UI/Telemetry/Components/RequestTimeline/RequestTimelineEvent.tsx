import React from 'react';
import {Event} from './RequestTimelineEvents';
import {Badge} from 'reactstrap';
import {TimelineEvent} from "../TimeLine";
import {ToolTipItem} from "@app/Components/ToolTipItem";

type RequestTimelineEventProps = Event & {
    lineKey:string
    onEventClick: () => void;
};

export class RequestTimelineEvent extends React.PureComponent<RequestTimelineEventProps, {}> {
    public render() {
        const {
            left,
            width,
            backgroundColor,
            isException,
            time,
            fullTimeMs,
            count,
            text,
            isSelected,
            onEventClick,
            lineKey,
            exceptionMessage
        } = this.props;
        return (
            <TimelineEvent classNames={isSelected ? ['event-selected'] : undefined} style={{marginLeft: left}}
                           onClick={onEventClick}>
                <div className="time" style={{width, backgroundColor}}></div>
                <div className="event-info">
                    {isException &&
                        <span className="time-bug" id={lineKey}>
                        <ToolTipItem text={exceptionMessage} targetId={lineKey}/>
                    </span>
                    }
                    <b>{time}ms</b>
                    {count > 1 && (
                        <>
                            <Badge color="danger" pill>{count}</Badge>
                            &nbsp;
                            <Badge color="info" pill>{fullTimeMs}ms</Badge>
                        </>
                    )}
                    &nbsp;
                    {text}
                </div>
            </TimelineEvent>
        );
    }
}
