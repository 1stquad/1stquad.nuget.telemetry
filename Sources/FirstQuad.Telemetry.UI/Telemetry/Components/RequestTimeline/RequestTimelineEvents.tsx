import * as React from 'react';

import { RequestTimelineEvent } from './RequestTimelineEvent';
import {TimelineItem} from "../../Models";
import {TimelineEventsContainer} from "../TimeLine";

type EventsProps = {
    events: Event[];
    onEventClick: (selectedEvent: TimelineItem) => void;
};

export type Event = {
    width: string;
    left: string;
    count: number;
    text: string;
    item: TimelineItem;
    time: string;
    fullTimeMs: string;
    isException: boolean;
    isSelected: boolean;
    backgroundColor?: string;
    exceptionMessage?: string;
};

export class RequestTimelineEvents extends React.PureComponent<EventsProps, {}> {
    public render() {
        const { events, onEventClick } = this.props;
        return (
            <TimelineEventsContainer>
                {this.props.children}

                {events.map((event, i) => (
                    <RequestTimelineEvent key={`event_${i}`} lineKey={`event_${i}`} {...event} onEventClick={() => onEventClick(event.item)} />
                ))}
            </TimelineEventsContainer>
        );
    }
}
