try
{
Write-Output "BUILDING BROADCAST FILES..."
npm run build-prod-broadcast
Write-Output "BUILD COMPLETED"

Write-Output "CREATING BROADCAST DESTINATION MOCK FILES..."
new-item -ItemType File -Path "..\dist\generated\js\browser-client.js" -Force | out-null
new-item -ItemType File -Path "..\dist\generated\js\browser-client.js.map" -Force | out-null
Write-Output "CREATED SUCCESSFULLY"

Write-Output "COPYING BROADCAST FILES INTO dist FOLDER..."
copy-item ".\build\static\js\main.js" "..\dist\generated\js\browser-client.js" -Force
copy-item ".\build\static\js\main.js.map" "..\dist\generated\js\browser-client.js.map" -Force
Write-Output "COPIED SUCCESSFULLY"

Write-Output "BUILDING TELEMETRY FILES..."
npm run build-prod-telemetry
Write-Output "BUILD COMPLETED"

Write-Output "CREATING TELEMETRY DESTINATION MOCK FILES..."
new-item -ItemType File -Path "..\dist\generated\js\telemetry.js" -Force | out-null
new-item -ItemType File -Path "..\dist\generated\js\telemetry.js.map" -Force | out-null
new-item -ItemType File -Path "..\dist\generated\css\telemetry.css" -Force | out-null
new-item -ItemType File -Path "..\dist\generated\css\telemetry.css.map" -Force | out-null
Write-Output "CREATED SUCCESSFULLY"

Write-Output "COPYING TELEMETRY FILES INTO dist FOLDER..."
copy ".\build\static\js\main.js" "..\dist\generated\js\telemetry.js" -Force
copy ".\build\static\js\main.js.map" "..\dist\generated\js\telemetry.js.map" -Force
copy ".\build\static\css\telemetry.css" "..\dist\generated\css\telemetry.css" -Force
copy ".\build\static\css\telemetry.css.map" "..\dist\generated\css\telemetry.css.map" -Force
Write-Output "COPIED SUCCESSFULLY"

Write-Output "DELETING build FOLDER WITH FILES..."
remove-item '.\build' -Recurse
Write-Output "DELETED SUCCESSFULLY"

cd ..\dist

Write-Output "CREATING GLOBAL RESOURCES ARCHIVE WITH FILES..."
Compress-Archive -Path generated\css\*, generated\js\*, bootstrap\css\*, bootstrap\js\*, index.html -DestinationPath Request_Telemetry_Zip.zip -Force
if (Test-Path -Path ..\FirstQuad.Telemetry\Request_Telemetry_Zip -PathType Leaf)
{
echo Y | xcopy "Request_Telemetry_Zip.zip" "..\FirstQuad.Telemetry\Request_Telemetry_Zip"
}
else
{
echo F | xcopy "Request_Telemetry_Zip.zip" "..\FirstQuad.Telemetry\Request_Telemetry_Zip"
}
del Request_Telemetry_Zip.zip
Write-Output "CREATED SUCCESSFULLY"

Write-Output "DELETING dist FOLDER WITH FILES..."
remove-item '.\generated' -Recurse
Write-Output "DELETED SUCCESSFULLY"


Write-Output "DONE"
}
catch
{
Write-Error -Message $_
}

pause